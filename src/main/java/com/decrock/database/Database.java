package com.decrock.database;

import com.decrock.app.Config;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

import static java.lang.Integer.min;

/**
 * Handles the relation with the Microsoft Access database
 */
public class Database {

    private final Logger LOGGER = Logger.getLogger("Database");
    private final int MAXINSERTROWS = 1;
    private Config config;
    private BufferedReader br;
    private String[] fieldsName;
    private List<Map<String, String>> linesToMerge = new ArrayList<Map<String, String>>();

    private SimpleDateFormat inFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat inFormat2 = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
    private SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Connection conn;

    public Database(File file) throws Exception {
        this.config = Config.getInstance();
        Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        String pathDB = "jdbc:ucanaccess://" + file.getAbsolutePath() + ";memory=true";
        conn = DriverManager.getConnection(pathDB);

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT TOP 1 * FROM " + config.getMainTableName());
        config.setDbStructure(getStructure(rs));
        config.setFieldList(getFieldList(rs));
        config.setDbDetailStructure(getDetailStructure(rs));
        stmt.close();
    }

    private List<String> getFieldList(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        List<String> fieldList = new ArrayList<>(rsmd.getColumnCount());
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            fieldList.add(rsmd.getColumnName(i));
        }
        return fieldList;
    }

    private Map<String, Integer> getStructure(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        Map<String, Integer> dataStructure = new HashMap<>();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            dataStructure.put(rsmd.getColumnName(i), rsmd.getColumnType(i));
        }
        return dataStructure;
    }

    private Map<String, String> getDetailStructure(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        Map<String, String> dataStructure = new HashMap<>();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {

            if (rsmd.getColumnType(i) == 12 && rsmd.getColumnDisplaySize(i) > 255)
                dataStructure.put(rsmd.getColumnName(i), "MEMO");
            else dataStructure.put(rsmd.getColumnName(i), rsmd.getColumnTypeName(i));
        }
        return dataStructure;
    }

    public boolean checkStructure(String[] fieldsList) {
        List<String> fieldsList2 = new ArrayList<>();
        Collections.addAll(fieldsList2, fieldsList);
        for (String field : fieldsList2) {
            if (!config.getFieldList().contains(field)) return false;
        }
        return true;
    }

    private void createTable(String tableName, Map<String, Integer> struct) throws SQLException {
        StringBuilder fields = new StringBuilder();

        for (String field : struct.keySet()) {
            if (!field.equals(config.getId_fiche())) {
                fields.append(",[").append(field).append("] ").append(config.getDbDetailStructure().get(field));
            }
        }
        String sql = "CREATE TABLE " + tableName + " (" + config.getId_fiche() + " AUTOINCREMENT NOT NULL PRIMARY KEY, " + fields.substring(1) + ");";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }

    private void insert(String table, Map<String, String> values) throws SQLException, ParseException {
        List<Map<String, String>> l = new ArrayList<>();
        l.add(values);
        insert(table, l);
    }

    private Date parseDate(String dateStr) {
        Date date;
        try {
            date = inFormat1.parse(dateStr);
        } catch (ParseException e) {
            try {
                date = inFormat2.parse(dateStr);
            } catch (ParseException ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return date;
    }

    private float parseFloat(String value) {
        try {
            return Float.valueOf(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private String formatDate(String dateStr) throws ParseException {
        Date date = parseDate(dateStr);
        if (date == null) return "NULL";
        return "#" + outFormat.format(date) + "#";
    }


    private String formatNumber(String value) {
        try {
            Float.valueOf(value);
            return value;
        } catch (NumberFormatException e) {
            return "NULL";
        }
    }

    private String formatString(String value) {
        if (value == null) return "";
        return value.replaceAll("'", "''");
    }

    private String buildInsertSQL(String table, List<Map<String, String>> values) throws ParseException {
        StringBuilder valuesString = new StringBuilder();
        StringBuilder fieldsString = new StringBuilder();
        for (String field : config.getFieldList()) {
            if (values.get(0).containsKey(field) && !field.equals(config.getId_fiche()))
                fieldsString.append(", [").append(field).append("]");
        }
        for (Map<String, String> hashMap : values) {
            StringBuilder singleValues = new StringBuilder();
            for (String field : config.getFieldList()) {
                if (hashMap.containsKey(field) && !field.equals(config.getId_fiche())) {
                    if (config.getDbStructure().get(field).equals(93)) {
                        singleValues.append(",").append(formatDate(hashMap.get(field)));
                    } else if (config.getDbStructure().get(field).equals(4)) {

                        singleValues.append(",").append(formatNumber(hashMap.get(field)));
                    } else {
                        singleValues.append(",'").append(formatString(hashMap.get(field))).append("'");
                    }
                }
            }
            if (singleValues.length() > 1) valuesString.append(", (").append(singleValues.substring(1)).append(")");
        }
        if (valuesString.length() > 0) return "INSERT INTO " + table + " (" + fieldsString.substring(2) + ") " +
                "VALUES " + valuesString.substring(1) + ";";
        return null;
    }

    private PreparedStatement buildInsertSQL(String table, Map<String, String> values) throws SQLException, ParseException {
        StringBuilder fieldsString = new StringBuilder();
        for (String field : config.getFieldList()) {
            if (values.containsKey(field) && !field.equals(config.getId_fiche()))
                fieldsString.append(", [").append(field).append("]");
        }
        StringBuilder valuesString = new StringBuilder();
        for (int i = 0; i < config.getFieldList().size(); i++) valuesString.append(",?");
        PreparedStatement pstmt = conn.prepareStatement(
                "INSERT INTO " + table + " (" + fieldsString.substring(2) + ") " +
                        "VALUES (" + valuesString.substring(1) + ");");
        int i = 1;
        for (String field : config.getFieldList()) {

            if (values.containsKey(field) && !field.equals(config.getId_fiche())) {
                if (config.getDbStructure().get(field).equals(93)) {
                    pstmt.setDate(i, (java.sql.Date) parseDate(values.get(field)));
                } else if (config.getDbStructure().get(field).equals(4)) {
                    pstmt.setFloat(i, parseFloat(values.get(field)));
                } else {
                    pstmt.setString(i, values.get(field));
                }
            }
            i++;
        }
        return null;
    }

    private void insert(String table, List<Map<String, String>> values) throws SQLException, ParseException {

        for (int i = 0; i < values.size(); i += MAXINSERTROWS) {
            String sql = buildInsertSQL(table, values.subList(i, min(i + MAXINSERTROWS, values.size())));
            if (sql == null) continue;
            LOGGER.info(sql);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
        }
    }

    private boolean isValidData(Map<String, String> data) {
        if (!data.containsKey(config.getDate_fiche())
                || !data.containsKey(config.getReference_fiche())
                || !data.containsKey(config.getEnqueteur_fiche()))
            return false;
        if (data.get(config.getDate_fiche()) == null
                || data.get(config.getReference_fiche()) == null
                || data.get(config.getEnqueteur_fiche()) == null) {
            return false;
        }
        return true;
    }

    void insertModif(Map<String, String> h) throws SQLException, ParseException {
        if (!isValidData(h)) return;

        String sql = "INSERT INTO " + config.getModifTableName() +
                " ([" + config.getDate_fiche() +
                "], [" + config.getReference_fiche() +
                "], [" + config.getEnqueteur_fiche() +
                "]) VALUES (" +
                formatDate(h.get(config.getDate_fiche())) + ",'" +
                h.get(config.getReference_fiche()) + "','" +
                h.get(config.getEnqueteur_fiche()) + "');";

        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }

    public void update(Map<String, String> hashMap) throws SQLException, ParseException {
        StringBuilder update = new StringBuilder();
        for (String field : hashMap.keySet()) {
            if (!field.equals(config.getReference_fiche()) && !field.equals(config.getId_fiche())) {
                if (hashMap.get(field).equals("")) update.append(", [").append(field).append("] = NULL");
                if (config.getDbStructure().get(field).equals(93)) {
                    update.append(", [").append(field).append("] = ").append(formatDate(hashMap.get(field)));
                } else if (config.getDbStructure().get(field).equals(4)) {
                    update.append(", [").append(field).append("] = ").append(formatNumber(hashMap.get(field)));
                } else {
                    update.append(", [").append(field).append("] = '").append(formatString(hashMap.get(field))).append("'");
                }
            }


        }
        String sql = "UPDATE " + config.getMainTableName() + " SET " + update.substring(2) + " " +
                "WHERE " + config.getId_fiche() + " = " + hashMap.get(config.getId_fiche()) + ";";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }


    public List<Map<String, String>> searchSheet(String id) throws SQLException {
        String sql = "SELECT * FROM " + config.getMainTableName()
                + " WHERE " + config.getReference_fiche() + " LIKE '%" + id + "%';";

        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        return searchSheetResults(rs);
    }

    public List<Map<String, String>> searchSheetByAdress(String address) throws SQLException {
        String sql = "SELECT * FROM " + config.getMainTableName()
                + " WHERE " + config.getAdresse_fiche() + " LIKE '%" + formatString(address) + "%';";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        return searchSheetResults(rs);
    }

    private List<Map<String, String>> searchSheetResults(ResultSet rs) throws SQLException {
        List<Map<String, String>> sheetsList = new ArrayList<Map<String, String>>();
        while (rs.next()) {
            Map<String, String> sheet = new HashMap<String, String>();
            sheet.put(config.getId_fiche(), rs.getString(config.getId_fiche()));
            for (String field : config.getFieldList()) {
                String value = rs.getString(field);
                sheet.put(field, value);
            }
            sheetsList.add(sheet);
        }
        return sheetsList;
    }

    public List<Map<String, String>> selectAll() throws SQLException {
        String sql = "SELECT * FROM " + config.getMainTableName() + ";";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        return searchSheetResults(rs);
    }

    private ResultSet getDoubleResultSet(Map<String, String> hashMap) throws SQLException {
        String sql = "SELECT * FROM " + config.getMainTableName() + " WHERE [" + config.getReference_fiche() + "] = '" +
                hashMap.get(config.getReference_fiche()) + "' AND [" + config.getEnqueteur_fiche() + "] != '" + hashMap.get(config.getEnqueteur_fiche()) + "';";
        LOGGER.info(sql);
        System.out.println(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        return rs;
    }

    public boolean checkDoubleSimple(Map<String, String> hashMap) throws SQLException {
        ResultSet rs = getDoubleResultSet(hashMap);
        if (rs.next()) {
            rs.close();
            return true;
        } else {
            rs.close();
            return false;
        }
    }


    public Map<String, String> checkDouble(Map<String, String> hashMap) throws SQLException {
        ResultSet rs = getDoubleResultSet(hashMap);
        if (rs.next()) {
            Map<String, String> duplicate = new HashMap<>();
            for (String field : config.getFieldList()) {
                String value = rs.getString(field);
                duplicate.put(field, value);
            }
            return duplicate;
        } else {
            return null;
        }
    }

    public String getPhotoInfos(Date date, String user) throws SQLException {
        String sql = "SELECT * FROM " + config.getModifTableName() + " WHERE " +
                config.getEnqueteur_fiche() + " = '" + user +
                "' AND " + config.getDate_fiche() + " <= #" + outFormat.format(date) +
                "# ORDER BY " + config.getDate_fiche() + " DESC";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        if (rs.next()) {
            return rs.getString(config.getReference_fiche());
        }
        return null;
    }

    public void addOrUpdate(Map<String, String> h) throws SQLException, ParseException {
        String sql = "SELECT * FROM " + config.getMainTableName() + " WHERE " + config.getReference_fiche() + " = '" + h.get(config.getReference_fiche()) + "';";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        if (rs.next()) {
            h.put(config.getId_fiche(), rs.getString(config.getId_fiche()));
            LOGGER.info("Updating entry " + h.get(config.getReference_fiche()));
            update(h);
        } else {
            LOGGER.info("Adding entry for " + h.get(config.getReference_fiche()));
            insert(config.getMainTableName(), h);
        }
    }

    public List<Map<String, String>> executeQuery(String query, String tableName) throws SQLException {
        LOGGER.info(query);


        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        Map<String, Integer> struct = getStructure(rs);
        List<Map<String, String>> values = new ArrayList<>();
        List<String> plotsList = new ArrayList<>();
        while (rs.next()) {
            Map<String, String> h = new HashMap<>();
            for (String key : struct.keySet()) {
                h.put(key, rs.getString(key));
            }
            plotsList.add(h.get(config.getReference_fiche()));
            values.add(h);
        }
        stmt.close();
        return values;
    }


    public void saveQueryInTable(String tableName, Map<String, Integer> structure, List<Map<String, String>> values) throws SQLException, ParseException {
        createTable(tableName, structure);
        insert(tableName, values);
    }

    public void close() throws SQLException {
        conn.close();
    }

    public Map<String, String> getNextSheet(int id) throws SQLException {
        String sql = "SELECT TOP 1 * FROM " + config.getMainTableName()
                + " WHERE " + config.getId_fiche() + " > " + id + ";";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        Map<String, String> sheet = getResultSheet(rs);
        return sheet;
    }

    public Map<String, String> getPreviousSheet(int id) throws SQLException {
        String sql = "SELECT TOP 1 * FROM " + config.getMainTableName()
                + " WHERE " + config.getId_fiche() + " < " + id + " ORDER BY " + config.getId_fiche() + " DESC;";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        Map<String, String> sheet = getResultSheet(rs);
        return sheet;
    }

    private Map<String, String> getResultSheet(ResultSet rs) throws SQLException {
        if (rs.next()) {
            Map<String, String> sheet = new HashMap<String, String>();
            for (String field : config.getFieldList()) {
                sheet.put(field, rs.getString(field));
            }
            return (sheet);
        }
        return null;
    }


    public List<String> getAllInvestigators() throws SQLException {
        String sql = "SELECT DISTINCT " + config.getEnqueteur_fiche() + " FROM " + config.getMainTableName()
                + ";";
        LOGGER.info(sql);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        List<String> investigatorList = new ArrayList<String>(3);
        while (rs.next()) {
            investigatorList.add(rs.getString(config.getEnqueteur_fiche()));
        }
        return investigatorList;
    }

}

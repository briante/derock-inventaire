package com.decrock.database;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

public class SheetManager {

    private static String splitRegex = "\\|";
    List<Map<String, String>> linesToMerge = new ArrayList<Map<String, String>>();
    private Database database;
    private String[] fieldsName;
    private Iterator<Map<String, String>> dataIterator;

    public SheetManager(File dbFile) throws Exception {
        database = new Database(dbFile);
    }

    public Database getDb() {
        return database;
    }

    public void readCSV(File csvFile) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        fieldsName = br.readLine().split(splitRegex);
        if (database.checkStructure(fieldsName)) {
            linesToMerge.clear();
            List<Map<String, String>> data = new ArrayList<Map<String, String>>();
            String line;
            while ((line = br.readLine()) != null) {
                String[] fields = line.split(splitRegex, -1);
                Map<String, String> h = new HashMap<String, String>();
                for (int i = 0; i < fieldsName.length; i++) {
                    String value = fields[i];
                    value = value.replaceAll(";\\s*(?=;)", "");
                    value = value.replaceAll("^(;|\\s)*;*", "");
                    value = value.replaceAll("(;|\\s)*$", "");
                    if (!value.equals("")) h.put(fieldsName[i], value);
                }
                data.add(h);
            }
            dataIterator = data.listIterator();
        } else throw new Exception("Erreur: La base de données n'a pas toutes les colones necessaires");
    }

    public void writeCSV(File file, List<String> fields) throws Exception {
        List<Map<String, String>> data = database.selectAll();

        String line = "";
        for (String field : fields) {
            line += "|" + field;
        }
        line = line.substring(1);
        Writer pw = new FileWriter(file);
        pw.write(line);
        pw.write("\n");
        for (Map<String, String> lineData : data) {
            line = "";
            for (String field : fields) {
                line += "|";
                if (lineData.get(field) != null) line += lineData.get(field);
            }
            pw.write(line.substring(1));
            pw.write("\n");
        }
        pw.close();
    }

    public int readDatabase(File file) throws Exception {
        Database db = new Database(file);
        List<Map<String, String>> data = db.selectAll();
        int doubleCount = 0;
        for (Map<String, String> h : data) {
            if (database.checkDoubleSimple(h)) doubleCount++;
        }
        linesToMerge.clear();
        dataIterator = data.listIterator();
        return doubleCount;
    }

    public void choose(Map<String, String> h) {
        linesToMerge.add(h);
    }

    public List<Map<String, String>> continueMerge() throws Exception {
        while (dataIterator.hasNext()) {
            Map<String, String> h = dataIterator.next();
            Map<String, String> duplicate = database.checkDouble(h);

            if (duplicate != null) {
                List<Map<String, String>> l = new ArrayList<Map<String, String>>();
                l.add(duplicate);
                l.add(h);
                return l;
            } else {
                linesToMerge.add(h);
            }
        }
        for (Map<String, String> data : linesToMerge) {
            database.addOrUpdate(data);
            database.insertModif(data);
        }
        return null;
    }

    public void cleanData() {
        dataIterator = null;
    }

    public void close() throws SQLException {
        if (database != null) {
            database.close();
        }
        database = null;
    }

    public List<Map<String, String>> searchSheet(String keyword) throws SQLException {
        List<Map<String, String>> data = null;
        if (keyword.trim().length() > 0) {
            data = database.searchSheet(keyword);
            if (data != null && !data.isEmpty()) return data;
            else return database.searchSheetByAdress(keyword);
        } else return null;
    }

    public Map<String, String> getPreviousSheet(int id) throws SQLException {
        return database.getPreviousSheet(id);
    }

    public Map<String, String> getNextSheet(int id) throws SQLException {
        return database.getNextSheet(id);
    }

    public List<String> getAllInvestigators() throws SQLException {
        return database.getAllInvestigators();
    }


}

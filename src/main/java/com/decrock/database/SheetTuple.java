package com.decrock.database;

public class SheetTuple {
    private int number;
    private String address;

    public SheetTuple(int number, String adress) {
        this.number = number;
        this.address = adress;
    }

    public SheetTuple(String number, String adress) {
        this.number = Integer.valueOf(number);
        this.address = adress;
    }

    public String getAddress() {
        return address;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return ("[" + number + ", " + address + "]");
    }
}

package com.decrock.gui;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Handle the pdf previews
 */
public class PDFViewer {

    public static String previewURI;
    private Image preview;

    public PDFViewer(File file) throws IOException {
        preview = getPDFPreview(file, true);

    }

    private PDFViewer(File file, boolean getPreview) throws IOException {
        getPDFPreview(file, getPreview);
    }

    public static void makePreview(File file) throws IOException {
        new PDFViewer(file, false);
    }

    /**
    Returns a list of all the pdf files
     **/
    public static List<File> getAllPDFFiles(File dir) {
        LinkedList<File> fileList = new LinkedList<File>();
        if (dir.exists()) {
            for (File pdfFile : dir.listFiles()) {
                if (pdfFile.getName().endsWith(".pdf")) {
                    fileList.add(pdfFile);
                }
            }
        }
        return fileList;
    }

    /**
    Returns the pdf preview
     **/
    private Image getPDFPreview(File file, boolean getPreview) throws IOException {
        File image = new File(previewURI + getPreviewName(file));
        if (image.exists()) {
            if (!getPreview) {
                return null;
            } else return new Image("file:" + image.getPath());
        } else {
            return makePDFPreview(file, getPreview);
        }
    }

    /**
    Create the pdf preview image and return it
     **/
    private Image makePDFPreview(File file, boolean returnImg) throws IOException {
        PDDocument document = PDDocument.load(file);
        PDFRenderer renderer = new PDFRenderer(document);
        BufferedImage pageImage = renderer.renderImage(0);
        File destFile = new File(previewURI + getPreviewName(file));
        ImageIO.write(pageImage, "png", destFile);
        document.close();
        if (returnImg) return SwingFXUtils.toFXImage(pageImage, null);
        else return null;
    }

    private String getPreviewName(File file) {
        return file.getName() + "_preview.png";
    }

    public Image getPreview() {
        return preview;
    }
}

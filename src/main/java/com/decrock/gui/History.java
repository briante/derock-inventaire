package com.decrock.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *  Handles the SQL requests history
 */
public class History {
    public static File historyFile;

    public static List<String> getHistory() throws FileNotFoundException {
        return getHistory(5);
    }

    public static List<String> getHistory(int num) throws FileNotFoundException {
        List<String> l = new ArrayList<String>();
        Scanner scanner = new Scanner(historyFile);
        String line;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (!(line.trim().equals("")) && !(line.trim().equals("\n"))) l.add(line);
        }
        Collections.reverse(l);
        return l.subList(0, Integer.min(num, l.size()));
    }

    public static void addToHistory(String elem) throws IOException {
        String newline = elem.replaceAll("\\n", " ") + "\n";
        FileWriter fw = new FileWriter(historyFile.getPath(), true); //the true will append the new data
        fw.write(newline); //appends the string to the file
        fw.close();
    }
}

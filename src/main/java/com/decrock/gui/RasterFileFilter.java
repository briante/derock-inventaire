package com.decrock.gui;

import java.io.File;
import java.io.FileFilter;

public class RasterFileFilter implements FileFilter {

    private final String[] okFileExtensions = new String[]{"jpg", "png", "tif", "ecw", "mbtiles"};

    public boolean accept(File file) {
        for (String extension : okFileExtensions) {
            if (file.getName().endsWith("." + extension)) {
                return true;
            }
        }
        return false;
    }
}

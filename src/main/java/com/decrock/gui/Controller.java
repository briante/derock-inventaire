package com.decrock.gui;

import com.decrock.app.Config;
import com.decrock.database.SheetManager;
import com.decrock.ged.FileDoc;
import com.decrock.ged.GED;
import com.decrock.ged.ImageManager;
import com.decrock.ged.ResultSet;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.swing.JMapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Main controller for the application, contains all the commands for the app to work
 * It would be wise to try to separate this into several files, and use several controllers
 */
public class Controller implements Initializable {
    private final Logger LOGGER = Logger.getLogger("controller");
    private final Logger DOCLOGGER = Logger.getLogger("GED");
    private final ContextMenu mapMenu = new ContextMenu();

    // Get all useful JavaFX elements of the app
    @FXML
    Stage stage;
    @FXML
    SplitPane mainPane;
    @FXML
    TextField rech;
    @FXML
    TabPane tabPane;
    @FXML
    Tab docTab;
    @FXML
    VBox searchResult;
    @FXML
    ToggleButton detailBtn;
    @FXML
    Tab sheetTab;
    @FXML
    FlowPane rasterPane;
    @FXML
    FlowPane shpPane;
    @FXML
    Label rasterPath;
    @FXML
    Label shpPath;
    @FXML
    VBox databaseMenu;
    @FXML
    HBox sheetPane;
    @FXML
    TextField searchSheetField;
    @FXML
    Button refSearchButton;
    @FXML
    VBox menuPane;
    @FXML
    TextArea queryField;
    @FXML
    TextField tableNameField;
    @FXML
    ChoiceBox investigatorNames;
    @FXML
    Label errorLabel;
    @FXML
    StackPane mapPane;
    @FXML
    Label documentationPath;
    @FXML
    Label photosPath;
    @FXML
    Label addressLabel;
    @FXML
    ColorPicker queryColorPicker;
    @FXML
    FlowPane mapControlsPane;
    @FXML
    TextField imageMapSize;
    @FXML
    Label dbPath;
    @FXML
    Label searchInfos;
    @FXML
    ToggleButton zoomToolButton;
    @FXML
    ChoiceBox saveExtension;
    @FXML
    CheckBox searchSheetDoc;

    //Gestion Electronique de Documents (GED)
    private GED ged;
    private Map<CheckBox, File> rasterReferences;
    private Map<CheckBox, File> shpReferences;
    private MapWindow mapWindow;
    private SheetManager sheetManager;
    private Config config;
    private List<VBox> docContents;
    private List<Label> docInfos;
    private int historyLevel = -1;
    private List<String> history;
    private String tmpHistory = "SELECT * FROM Fiches WHERE";
    private MenuItem searchDoc;
    private MenuItem searchLargeDoc;
    private MenuItem openSheet;
    private MenuItem titleItem = new MenuItem();
    private Sheet sheet;
    private CursorTool selectTool;
    private CursorTool zoomTool;
    private CursorTool actualTool;
    private NumberFormat positionFormatter = new DecimalFormat("#0.0000");
    private double oldHeight = 0;
    private Text textHolder = new Text();
    private List<Map<String, String>> sheetList = null;
    private int sheetListIdx = 0;
    private Map<String, String> oldSheet;

    /**
     * Initialize the controller and the display of the application
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialization of useful items
        ged = new GED();
        try {
            mapWindow = MapWindow.initMap();
        } catch (Exception e) {
            printError(e);
            e.printStackTrace();
        }
        docContents = new ArrayList<>();
        docInfos = new ArrayList<>();
        rasterReferences = new HashMap<>();
        shpReferences = new HashMap<>();

        // Create the right click contextual menu for the map panel
        searchDoc = new MenuItem("Recherche de documents");
        searchDoc.setOnAction(event -> Platform.runLater(() -> searchDocFromPlot(mapWindow.getActiveAddress())));
        searchLargeDoc = new MenuItem("Recherche de documents élargie");
        searchLargeDoc.setOnAction(event -> Platform.runLater(() -> searchDocFromPlotLarge(mapWindow.getActiveAddress())));
        openSheet = new MenuItem("Ouvrir la fiche");
        openSheet.setOnAction(event -> Platform.runLater(() -> searchSheet(mapWindow.getActivePlot())));

        // Get the configuration
        config = Config.getInstance();

        // Write the paths of the documentation and the photos
        setPathDisplay(documentationPath, config.getDocumentation_path());
        setPathDisplay(photosPath, config.getPhotos_path());

        initRasterFiles();
        initShpFiles();

        showMap();
        selectTool = this.createCursorTool();
        zoomTool = this.createZoomTool();

        try {
            history = History.getHistory();
        } catch (FileNotFoundException e) {
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
        openDB();
        titleItem.getStyleClass().add("context-menu-title");
        mapMenu.getItems().add(0, titleItem);
        saveExtension.setItems(FXCollections.observableArrayList(
                "png", "tiff"));
        saveExtension.getSelectionModel().selectFirst();

        textHolder.textProperty().bind(queryField.textProperty());
        textHolder.layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            if (oldHeight != newValue.getHeight()) {
                oldHeight = newValue.getHeight();
                queryField.setMinHeight(textHolder.getLayoutBounds().getHeight() + 20); // +20 is for paddings
            }
        });
        queryColorPicker.setValue(Color.RED);

    }

    /**
     * Prints an error at the bottom of the menu, in the "errorLabel" element
     * @param e
     */
    private void printError(Exception e) {
        errorLabel.setTextFill(Color.RED);
        errorLabel.setText(e.getMessage());
    }

    /**
     * Prints an error at the bottom of the menu, in the "errorLabel" element
     * @param s
     */
    private void printError(String s) {
        errorLabel.setTextFill(Color.RED);
        errorLabel.setText(s);
    }

    /**
     * Prints a message at the bottom of the menu, in the "errorLabel" element
     * @param e
     */
    private void printMsg(String e) {
        errorLabel.setTextFill(Color.BLACK);
        errorLabel.setText(e);
    }

    /**
     * Format a path to display in the app: paths that are too long are cropped
     * @param path
     * @return
     */
    private String formatPath(String path) {
        if (path.length() < 25) return path;
        else
            return (path.substring(0, 3) + "..." + path.substring(path.length() - new File(path).getName().length() - 1));
    }

    /**
     * Display a path in the given label, if the path is of length 0 (not set), display the given message,
     * usually "Please select file"...
     * @param label
     * @param path
     * @param msg
     */
    private void setPathDisplay(Label label, String path, String msg) {
        if (path.trim().length() < 5) {
            label.setText(msg);
        } else {
            label.setText(formatPath(path));
        }
    }


    private void setPathDisplay(Label label, String path) {
        setPathDisplay(label, path, "Sélectionner le dossier");
    }

    //*********************************************
// GED
//*********************************************

    /**
     * Change the location of the Documents (Activated by clicking on the path (this is set up in the FXML file)
     */
    @FXML
    private void changeDocLocation() {
        clearError();
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Sélectionner l'emplacement de la documentation");
        if (config.getDocumentation_path().trim().length() > 0 && new File(config.getDocumentation_path()).exists()) {
            fileChooser.setInitialDirectory(new File(config.getDocumentation_path()));
        }

        File file = fileChooser.showDialog(stage);
        if (file != null) {
            try {
                config.setDocumentationPath(file.getAbsolutePath());
            } catch (IOException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
            documentationPath.setText(formatPath(config.getDocumentation_path()));
        }
    }

    /**
     * Search for documentation about a series of keywords and display it. the search is handle by the GED class and the display
     * by another method
     */
    @FXML
    private void searchDoc() {
        clearError();
        if (!rech.getText().trim().isEmpty()) {
            printMsg("Recherche en cours");
            detailBtn.setSelected(false);
            try {
                ResultSet resultSet = ged.searchDoc(rech.getText().split(";"));
                displayDoc(resultSet);
            } catch (IOException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        } else {
            printError("Le champs de recherche est vide");
        }
    }

    /**
     * Display the documentation and set it active (meaning the documents will be unfolded with all the details visible
     * @param resultSet
     * @throws IOException
     */
    private void displayDoc(ResultSet resultSet) throws IOException {
        displayDoc(resultSet, true);
    }

    /**
     * Display the result of a documentation search
     * @param resultSet
     * @throws IOException
     */
    private void displayDoc(ResultSet resultSet, boolean setActive) throws IOException {
        if (resultSet == null) return;

        DOCLOGGER.debug(resultSet + " " + resultSet.getResults());

        detailBtn.setSelected(false);
        // clear the previous search
        searchResult.getChildren().clear();
        // Write the search's information
        searchInfos.setText(resultSet.toString());
        // Get the results as a list
        List<FileDoc> filesDocList = resultSet.getResults();
        for (final FileDoc f : filesDocList) {
            try {
                // Create the document display pane
                TitledPane filePane = new TitledPane();
                filePane.setWrapText(true);
                filePane.setAnimated(false);
                // If the document is a file, the title is the name of the file, else it is the document field "REF"
                if (f.getFile() != null) {
                    filePane.setText(f.getFile().getName());
                } else {
                    filePane.setText(f.getRef());
                }
                // Button to open the document
                VBox buttons = new VBox();
                Button open = new Button("Ouvrir le document");
                buttons.getChildren().add(open);
                buttons.setSpacing(10);
                // Write the "REF" field
                String infosText = "REF : " + f.getRef();
                if (!f.getPage().equals("")) infosText += "\nPAGE : " + f.getPage();

                // Write the "MCL" field
                infosText += "\nMCL : " + f.getMcl();
                Label infos = new Label(infosText);
                infos.setPrefWidth(config.getPreview_size());
                infos.setWrapText(true);
                docInfos.add(infos);
                VBox infoBox = new VBox();
                docContents.add(infoBox);
                // Write the "TXT" field if it exists
                Label txtLabel = new Label(f.getTxt().equals("") ? "" : "TXT : " + f.getTxt());
                txtLabel.setPrefWidth(config.getPreview_size());
                txtLabel.setWrapText(true);
                VBox content = new VBox(infoBox, txtLabel);
                DOCLOGGER.debug("added text content");
                // If the document has a file, try to display it
                if (f.getFile() != null) {
                    DOCLOGGER.debug("result has a file to display: " + f.getFile().getName());
                    // Set the open file button to open the file
                    open.setDisable(false);
                    open.setOnAction(event -> openDocFile(f.getFile()));
                    String fileName = f.getFile().getName().toLowerCase();

                    // If the file is an image or a pdf, display it
                    if (fileName.endsWith("jpg") || fileName.endsWith("png") || fileName.endsWith("pdf")) {
                        Image image;
                        if (fileName.endsWith("pdf")) {
                            DOCLOGGER.debug("getting preview image for: " + fileName);
                            // If pdf, get the preview
                            PDFViewer pdfViewer = new PDFViewer(f.getFile());
                            image = pdfViewer.getPreview();
                            DOCLOGGER.debug("got preview image: " + image.toString());
                        } else {
                            DOCLOGGER.debug("displaying image: " + fileName);
                            // if image, display it directly
                            image = new Image("file:" + f.getFile().getPath());
                        }
                        ImageView iv = new ImageView();
                        iv.setFitWidth(config.getPreview_size());
                        iv.setFitHeight(config.getPreview_size());
                        iv.setPreserveRatio(true);
                        iv.setImage(image);
                        content.getChildren().add(iv);
                    }

                    content.getChildren().add(open);
                }

                filePane.setContent(content);
                filePane.setExpanded(true);
                // Add the document display to the list
                searchResult.getChildren().add(filePane);
                DOCLOGGER.debug("result added " + f);
            } catch (Exception e) {
                e.printStackTrace();
                printError(e);
                DOCLOGGER.error(e.toString());
            }

        }
        // Set the title of the tab to show the number of matches
        docTab.setText(String.format("Documentation (%s)", String.valueOf(filesDocList.size())));
        if (setActive) tabPane.getSelectionModel().select(docTab);
        DOCLOGGER.debug(searchResult.getChildren().size() + " results for  search " + resultSet.toString());
        printMsg("Recherche correctement effectuée");
    }


    // Open a file
    @FXML
    private void openDocFile(File file) {
        clearError();
        try {
            ged.openFile(file);
        } catch (IOException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Perform a document search from the address of a plot ("parcelle")
     */

    private void searchDocFromPlot(String address) {
        try {
            ResultSet resultSet = ged.getPlotDoc(address);
            displayDoc(resultSet);
        } catch (IOException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Perform a large document search from the address of a plot ("parcelle"). large means it doee not consider the
     * number of the building
     */
    private void searchDocFromPlotLarge(String address) {
        try {
            ResultSet resultSet = ged.getPlotDocLarge(address);
            displayDoc(resultSet);
        } catch (IOException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    @FXML
    private void setDocInfos() {
        clearError();
        setDocInfos(detailBtn.isSelected());
    }

    private void setDocInfos(boolean b) {
        for (int i = 0; i < this.docContents.size(); i++) {
            try {
                docContents.get(i).getChildren().clear();
                if (b) docContents.get(i).getChildren().add(docInfos.get(i));
            } catch (Exception e) {
                LOGGER.error(e.toString());
                e.printStackTrace();
            }

        }
    }

//*********************************************
// Affichage de la Carte
//*********************************************

    /**
     * Look for the Shp files in the shp directory
     */
    private void initShpFiles() {
        shpReferences.clear();
        shpPane.getChildren().clear();
        File shpDirectory = new File(config.getShp_path());
        if (shpDirectory.exists()) {
            for (File f : shpDirectory.listFiles(new ShpFileFilter())) {
                CheckBox cb = new CheckBox(f.getName());
                shpReferences.put(cb, f);
                shpPane.getChildren().add(cb);
            }
        }
        setPathDisplay(shpPath, config.getShp_path());
    }

    /**
     * Look for the rasters files (TIFF, ECW, etc) in the raster directory
     */
    private void initRasterFiles() {
        rasterReferences.clear();
        rasterPane.getChildren().clear();
        File rasterDirectory = new File(config.getRaster_path());
        if (rasterDirectory.exists()) {
            for (File f : rasterDirectory.listFiles(new RasterFileFilter())) {
                CheckBox cb = new CheckBox(f.getName());
                rasterReferences.put(cb, f);
                rasterPane.getChildren().add(cb);
            }
        }
        setPathDisplay(rasterPath, config.getRaster_path());
    }


    private List<File> getActiveRasters() {
        List<File> files = new ArrayList<File>();
        for (CheckBox cb : rasterReferences.keySet()) {
            if (cb.isSelected()) files.add(rasterReferences.get(cb));
        }
        return files;
    }

    private List<File> getActiveShp() {
        List<File> files = new ArrayList<File>();
        for (CheckBox cb : shpReferences.keySet()) {
            if (cb.isSelected()) files.add(shpReferences.get(cb));
        }
        return files;
    }


    /**
     * Change the path of the raster directory (Activated by clicking on the path, this is set up in the FXML file)
     */
    @FXML
    private void changeRasterPath() {
        clearError();
        DirectoryChooser fileChooser = new DirectoryChooser();
        if (config.getRaster_path().trim().length() > 0 && new File(config.getRaster_path()).exists()) {
            fileChooser.setInitialDirectory(new File(config.getRaster_path()));
        }
        fileChooser.setTitle("Sélectionner l'emplacement des rasters");
        File file = fileChooser.showDialog(stage);
        if (file != null) {
            try {
                config.setRaster_path(file.getAbsolutePath());
            } catch (IOException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
            initRasterFiles();
        }
    }

    /**
     * Change the path of the shp directory (Activated by clicking on the path, this is set up in the FXML file)
     */
    @FXML
    private void changeShpPath() {
        clearError();
        DirectoryChooser fileChooser = new DirectoryChooser();
        if (config.getShp_path().trim().length() > 0 && new File(config.getShp_path()).exists()) {
            fileChooser.setInitialDirectory(new File(config.getShp_path()));
        }
        fileChooser.setTitle("Sélectionner l'emplacement des fichiers shp");
        File file = fileChooser.showDialog(stage);
        if (file != null) {
            try {
                config.setShp_path(file.getAbsolutePath());
            } catch (IOException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
            initShpFiles();
        }
    }

    /**
     * Create the cursor tool of for interacting with the map
     * @return
     */
    private CursorTool createCursorTool() {
        return new CursorTool() {
            @Override
            public void onMouseClicked(final MapMouseEvent ev) {
                Platform.runLater(() -> {
                    mapMenu.hide();
                    addressLabel.setText("");
                });
                mapWindow.getMousePos(ev);
                if (ev.getButton() == 1 || ev.getButton() == 3) {
                    Platform.runLater(mapMenu::hide);
                    try {
                        mapWindow.selectFeatures(ev);
                    } catch (IOException e) {
                        printError(e);
                        LOGGER.error(e.toString());
                        e.printStackTrace();
                    }
                    Platform.runLater(() -> {
                        mapMenu.hide();
                        if (!mapWindow.getActiveAddress().equals(""))
                            addressLabel.setText(String.format("%s (Parcelle %s)", mapWindow.getActiveAddress(), mapWindow.getActivePlot()));
                        if (ev.getButton() == 3) showMapMenu(ev);
                    });
                }
            }

            @Override
            public void onMousePressed(MapMouseEvent ev) {
                Platform.runLater(mapMenu::hide);
                mapWindow.startPanning(ev);
            }

            @Override
            public void onMouseDragged(MapMouseEvent ev) {
                mapWindow.pan(ev);
            }

            @Override
            public void onMouseReleased(MapMouseEvent ev) {
                mapWindow.endPanning(ev);
            }
        };
    }

    /**
     * Create the zoom tool for zooming with rectangle
     * @return
     */
    private CursorTool createZoomTool() {
        return new CursorTool() {
            @Override
            public void onMousePressed(MapMouseEvent ev) {
                mapWindow.setRectangleStart(ev);
            }

            @Override
            public void onMouseDragged(MapMouseEvent ev) {
            }

            @Override
            public void onMouseReleased(MapMouseEvent ev) {
                mapWindow.setRectangleEnd(ev);
                mapWindow.zoomByRectangle();
                changeTool();
                zoomToolButton.setSelected(false);
            }
        };
    }

    @FXML
    private void zoomOut() {
        mapWindow.zoomOut();
    }

    @FXML
    private void zoomIn() {
        mapWindow.zoomIn();
    }

    @FXML
    private void zoomReset() {
        mapWindow.resetZoom();
    }

    /**
     * Update the display of the map, adding the selected raster and shp
     */
    @FXML
    private void updateMap() {
        clearError();
        List<File> rasterFiles = this.getActiveRasters();
        List<File> shpFiles = this.getActiveShp();
        mapWindow.getMapPane().setCursorTool(this.selectTool);
        if (shpFiles != null || rasterFiles != null) {
            Platform.runLater(() -> {
                try {
                    mapWindow.showMap(rasterFiles, shpFiles);
                    mapControlsPane.getChildren().clear();
                    actualTool = selectTool;
                    for (int i = 0; i < rasterFiles.size(); i++) {
                        MapLayerController.getMapLayerController(mapWindow, mapControlsPane, rasterFiles.get(i).getName(), i, true);
                    }
                    for (int i = 0; i < shpFiles.size(); i++) {
                        MapLayerController.getMapLayerController(mapWindow, mapControlsPane, shpFiles.get(i).getName(), rasterFiles.size() + i, false);
                    }
                    mapWindow.resetZoom();
                } catch (Exception e) {
                    printError(e);
                    e.printStackTrace();
                }
            });
        }
    }

    @FXML
    private void resetMap() {
        mapWindow.dispose();
        try {
            mapWindow = MapWindow.initMap();
        } catch (Exception e) {
            printError(e);
            e.printStackTrace();
        }
        updateMap();
    }

    @FXML
    private void showMap() {
        clearError();
        try {
            SwingNode swingNode = new SwingNode();
            JMapPane map = mapWindow.getMapPane();
            swingNode.setContent(map);
            mapPane.getChildren().add(swingNode);
            if (!mapMenu.getItems().contains(searchDoc)) mapMenu.getItems().add(searchDoc);
            if (!mapMenu.getItems().contains(searchLargeDoc)) mapMenu.getItems().add(searchLargeDoc);
            mapWindow.getMapPane().setCursorTool(this.selectTool);
            actualTool = selectTool;
            mapControlsPane.getChildren().clear();
        } catch (Exception e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * CHange the tool used (select or zoom), handle by the toolbar buttons in the map panel (this is set up in the FXML file)
     */
    @FXML
    private void changeTool() {
        clearError();
        if (actualTool == zoomTool) {
            actualTool = selectTool;
            mapWindow.getMapPane().setCursorTool(this.selectTool);
        } else {
            actualTool = zoomTool;
            mapWindow.getMapPane().setCursorTool(this.zoomTool);
        }

    }

    /**
     * Show the contextual menu when clicking on a parcel in the map
     * @param event
     */
    private void showMapMenu(MapMouseEvent event) {
        if (mapWindow.hasSelectedFeature()) {
            DirectPosition2D mapPos = event.getWorldPos();
            titleItem.setText(String.format("X: %s, Y : %s", positionFormatter.format(mapPos.getX()), positionFormatter.format(mapPos.getY())));
            titleItem.setDisable(true);


            mapMenu.show(mapPane, event.getXOnScreen(), event.getYOnScreen());
        }
    }

    /**
     * Save the map into an image file
     */
    @FXML
    private void saveImage() {
        clearError();
        String extension = (String) saveExtension.getValue();
        if (!imageMapSize.getText().trim().isEmpty()) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Sélectionner un fichier");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(extension.toUpperCase(), "*." + extension.toLowerCase()));
            File file = fileChooser.showSaveDialog(stage);
            mapWindow.saveImage(file.getPath(),
                    Integer.valueOf(imageMapSize.getText()), extension.toLowerCase());
        }
    }


//*********************************************
// Gestion Database + fiches
//*********************************************

    /**
     * Change the database used for the parcels sheets
     * @throws IOException
     */
    @FXML
    private void changeDB() throws IOException {
        clearError();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sélectionner un fichier");
        if (config.getDb_path().trim().length() > 0) {
            File dir = new File(config.getDb_path());
            if (dir.isFile()) dir = dir.getParentFile();
            if (dir.exists()) fileChooser.setInitialDirectory(dir);
        }
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Base de Données Microsoft Access", "*.mdb", "*.accdb"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            String db_path = file.getAbsolutePath();
            config.setDb_path(db_path);
            openDB();
        }

    }

    /**
     * Open the database
     */
    private void openDB() {
        String db_path = config.getDb_path();
        setPathDisplay(dbPath, db_path, "Sélectionner la base de données");
//        dbPath.setText(formatPath(db_path));
        if (sheetManager != null) {
            try {
                sheetManager.close();
            } catch (SQLException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        }
        if (db_path.endsWith(".mdb") || db_path.endsWith(".accdb")) {
            File file = new File(db_path);
            if (file.exists()) {
                try {
                    sheetManager = new SheetManager(file);
                    databaseMenu.setDisable(false);
                    updateInvestigators();
                    if (!mapMenu.getItems().contains(openSheet)) mapMenu.getItems().add(openSheet);
                } catch (Exception e) {
                    printError(e);
                    LOGGER.error(e.toString());
                    e.printStackTrace();
                    databaseMenu.setDisable(true);
                }
            }
        }
    }


    private void updateInvestigators() {
        try {
            List<String> investigatorsList = sheetManager.getAllInvestigators();
            investigatorNames.setItems(FXCollections.observableArrayList(investigatorsList));
        } catch (SQLException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Export the database to csv
     */
    @FXML
    private void exportDb() {
        clearError();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sélectionner un fichier");
        File dir = new File(config.getDb_path());
        if (dir.isFile()) dir = dir.getParentFile();
        if (dir.exists()) fileChooser.setInitialDirectory(dir);
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            try {
                sheetManager.writeCSV(file, config.getFieldList());
            } catch (Exception e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Search for a sheet using using the search field
     */
    @FXML
    private void searchSheet() {
        clearError();
        if (searchSheetField.getText().trim().length() > 0) {
            searchSheet(searchSheetField.getText());
        }
    }

    private void searchSheet(String keyword) {
        sheetPane.getChildren().clear();
        List<Map<String, String>> data;
        try {
            data = sheetManager.searchSheet(keyword);
            if (data != null) {
                displaySheet(data);
            } else this.printError("Aucune fiche trouvée");
        } catch (SQLException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Save the modifications made to a sheet
     */
    private void saveSheet() {
        clearError();
        try {
            Map<String, String> saveData = new HashMap<String, String>();
            Map<String, String> newSheet = sheet.getSheetContent();
            for (String key : newSheet.keySet()) {
                if (!oldSheet.get(key).trim().equals(newSheet.get(key).trim())) {
                    saveData.put(key, newSheet.get(key));
                }
            }
            saveData.put(config.getId_fiche(), newSheet.get(config.getId_fiche()));
            sheetManager.getDb().update(saveData);
            printMsg("Fiche enregistrée");
        } catch (UnsupportedOperationException e) {
            LOGGER.error(e.toString());
            e.printStackTrace();
        } catch (SQLException e) {
//            printError(e);
//            LOGGER.error(e.toString());
//            e.printStackTrace();
        } catch (ParseException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }

    }

    /**
     * Get the documentation fora sheet
     * @param adress
     */
    private void getSheetDoc(String adress) {
        try {
            ResultSet doc = ged.getPlotDoc(adress);
            displayDoc(doc, false);
        } catch (IOException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Display a list of parcel sheets
     * @param data
     */
    private void displaySheet(List<Map<String, String>> data) {
        clearSheets();
        if (data.size() == 1) {
            this.displaySheet(data.get(0));
            sheetList = null;
        } else if (data.size() > 1) {
            this.chooseSheetToOpen(data);
            sheetList = data;
        }
    }

    /**
     * Display a parcel sheet
     * @param data
     */
    private void displaySheet(Map<String, String> data) {
        oldSheet = new HashMap<>();
        for (String key : data.keySet()) {
            oldSheet.put(key, data.get(key) == null ? "" : String.valueOf(data.get(key)));
        }
        sheetPane.getChildren().clear();

        VBox vbox = new VBox();
        vbox.setSpacing(10);
        vbox.setFillWidth(true);
        vbox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        vbox.setPrefWidth(1920);
        HBox buttons = new HBox(getSaveSheetButton()
                , getCancelSheetButton()
                , getPreviousSheetButton()
                , getNextSheetButton());
        buttons.setSpacing(10);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        scrollPane.setFitToWidth(true);
        scrollPane.setPannable(true);
        sheet = Sheet.buildSheet(scrollPane, true);
        sheet.setSheetContent(data);
        sheet.setDate();
        vbox.getChildren().addAll(buttons, new Separator(), scrollPane);
        sheetPane.getChildren().add(vbox);
        tabPane.getSelectionModel().select(sheetTab);

        if (searchSheetDoc.isSelected()) {
            getSheetDoc(data.get(config.getAdresse_fiche()));
        }

    }

    /**
     * If a sheet search returned several results, display a button for each result, each button open the result it is
     * associated to
     * @param data
     */
    private void chooseSheetToOpen(List<Map<String, String>> data) {
        FlowPane container = new FlowPane();
        container.setHgap(10);
        container.setVgap(10);
        for (int i = 0; i < data.size(); i++) {
            Button btn = new Button(data.get(i).get(config.getReference_fiche()));
            int finalI = i;
            btn.setOnAction(event -> {
                clearError();
                clearSheets();
                sheetListIdx = finalI;
                displaySheet(data.get(finalI));
            });
            container.getChildren().add(btn);
        }
        sheetPane.getChildren().add(container);
        tabPane.getSelectionModel().select(sheetTab);
    }


    private Button getPreviousSheetButton() {
        Button btn = new Button("<");
        btn.setOnAction(event -> {
            clearError();
            getPreviousSheet();
        });
        return btn;
    }

    private Button getNextSheetButton() {
        Button btn = new Button(">");
        btn.setOnAction(event -> {
            clearError();
            getNextSheet();
        });
        return btn;
    }

    private Button getSaveSheetButton() {
        Button saveBtn = new Button("Enregistrer Fiche");
        saveBtn.setOnAction(event -> {
            saveSheet();
        });
        return saveBtn;
    }

    private Button getCancelSheetButton() {
        Button cancelBtn = new Button("Annuler Modification");
        cancelBtn.setOnAction(event -> {
            clearError();
            clearSheets();
        });
        return cancelBtn;
    }

    /**
     * Open a file to merge into the database, detect the conflicts, and perform the merge
     */
    @FXML
    private void openMergeFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sélectionner un fichier");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Fichier", "*.csv", "*.mdb", "*.accdb"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                if (file.getName().endsWith(".csv")) {
                    sheetManager.readCSV(file);
                    merge();
                } else {
                    int doubleCount = sheetManager.readDatabase(file);
                    VBox vbox = new VBox();
                    vbox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
                    Button validation = new Button("Commencer la fusion");
                    validation.setOnAction(event -> {
                        sheetPane.getChildren().clear();
                        merge();
                    });
                    Button cancel = new Button("Annuler");
                    cancel.setOnAction(event -> {
                        sheetManager.cleanData();
                        sheetPane.getChildren().clear();
                    });
                    vbox.getChildren().addAll(new Label("Nombre de conflit détectés: " + doubleCount),
                            validation, cancel);
                    sheetPane.getChildren().add(vbox);
                }
                updateInvestigators();
            } catch (Exception e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Display a parcel sheet
     * @param chooseBtn
     * @param sheetData
     * @return
     */
    private VBox displaySheetChoice(Button chooseBtn, Map<String, String> sheetData) {
        VBox sheetChooserPane = new VBox(chooseBtn, new Separator());
        sheetChooserPane.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        sheetChooserPane.setFillWidth(true);
        sheetChooserPane.setSpacing(5);
        sheetChooserPane.setPadding(new Insets(5));
        sheetChooserPane.setAlignment(Pos.TOP_CENTER);
        sheetChooserPane.getStyleClass().add("sheetDouble");

        Sheet.buildSheet(sheetChooserPane, false).setSheetContent(sheetData);

        chooseBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                e -> showBorders(sheetChooserPane, true));

        chooseBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                e -> showBorders(sheetChooserPane, false));

        return sheetChooserPane;
    }

    /**
     * Display a parcel sheet conflict and ask to choose which one to add to the database
     * @param sheets
     */
    private void chooseDouble(List<Map<String, String>> sheets) {
        Button choose1 = new Button("Choisir Fiche");
        choose1.setOnAction(event -> {
            clearSheets();
            merge();
        });
        Button choose2 = new Button("Choisir Fiche");
        choose2.setOnAction(event -> {
            clearSheets();
            sheetManager.choose(sheets.get(1));
            merge();
        });

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        scrollPane.setFitToWidth(true);
        scrollPane.setPannable(true);

        HBox hbox = new HBox();
        hbox.setFillHeight(true);
        hbox.setSpacing(5);
        hbox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);

        hbox.getChildren().addAll(displaySheetChoice(choose1, sheets.get(0))
                , new Separator(Orientation.VERTICAL)
                , displaySheetChoice(choose2, sheets.get(1)));
        scrollPane.setContent(hbox);
        sheetPane.getChildren().add(scrollPane);
    }

    private void showBorders(Pane p, boolean show) {
        p.setStyle("-fx-border-color: " + (show ? "rgb(0,0,0,0.1);" : "transparent;"));
    }


    /**
     * Merge the
     */
    private void merge() {
        List<Map<String, String>> sheets = null;
        try {
            sheets = sheetManager.continueMerge();
        } catch (Exception e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
        if (sheets != null) {
            chooseDouble(sheets);
        }
    }

    private void clearSheets() {
        sheetPane.getChildren().clear();
    }

    /**
     * Get the sheet of the previous parcel in the database
     */
    private void getPreviousSheet() {
        saveSheet();
        if (sheetList != null) {
            if (sheetListIdx > 0) displaySheet(sheetList.get(--sheetListIdx));
        } else try {
            Map data = sheetManager.getPreviousSheet(sheet.getId());
            if (data != null) {
                displaySheet(data);
            } else {
                printError("Pas de fiche avant");
            }
        } catch (SQLException e) {
            LOGGER.error(e.toString());
            printError(e);
            e.printStackTrace();
        }
    }

    /**
     * Get the sheet of the next parcel in the database
     */
    private void getNextSheet() {
        saveSheet();
        if (sheetList != null) {
            if (sheetListIdx < sheetList.size() - 2) displaySheet(sheetList.get(++sheetListIdx));
        } else try {
            Map data = sheetManager.getNextSheet(sheet.getId());
            if (data != null) {
                displaySheet(data);
            } else {
                printError("Pas de fiche après");
            }
        } catch (SQLException e) {
            LOGGER.error(e.toString());
            printError(e);
            e.printStackTrace();
        }
    }

    @FXML
    private void classPhoto() {
        clearError();
        if (((String) investigatorNames.getValue()).trim().isEmpty()) {
            printError("Selectionez un enquèteur");
            return;
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selectionner un fichier");
        List<File> filesList = fileChooser.showOpenMultipleDialog(stage);
        if (filesList != null) {
            ImageManager im = new ImageManager(sheetManager.getDb());
            try {
                im.classFiles(filesList, (String) investigatorNames.getValue(), config.getPhotos_path());
            } catch (Exception e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void changePhotoLocation() {
        clearError();
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Selectionner l'emplacement où classer les photos");
        if (config.getPhotos_path().trim().length() > 0 && new File(config.getPhotos_path()).exists()) {
            fileChooser.setInitialDirectory(new File(config.getPhotos_path()));
        }
        File file = fileChooser.showDialog(stage);
        if (file != null) {
            try {
                config.setPhotos_path(file.getAbsolutePath());
            } catch (IOException e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
            setPathDisplay(photosPath, config.getPhotos_path());
        }
    }

    private String formatQueryString(String query) {
        return query.replaceAll("“|”", "\"").replaceAll("‘|’", "'");
    }


    /**
     * Execute the query written in the query field and display the results on the map
     */
    @FXML
    private void executeQuery() {
        clearError();
        String sql = formatQueryString(queryField.getText());
        if (sql.length() > 0 && sql.toLowerCase().startsWith(("SELECT * FROM " + config.getMainTableName()).toLowerCase())) {
            try {
                History.addToHistory(queryField.getText());
                String tableName = tableNameField.getText();
                List<Map<String, String>> values = sheetManager.getDb().executeQuery(sql, tableName);
                if (tableNameField.getText().trim().length() > 1 && values.size() > 0)
                    sheetManager.getDb().saveQueryInTable(tableName, config.getDbStructure(), values);

                if (values.size() > 0) {
                    displaySheet(values);
                    List<String> plotsList = new ArrayList<>(values.size());
                    for (Map<String, String> value : values) {
                        plotsList.add(value.get(config.getReference_fiche()));
                    }
                    Color fxColor = queryColorPicker.getValue();
                    try {
                        mapWindow.selectPlots(plotsList, new java.awt.Color((float) fxColor.getRed(),
                                (float) fxColor.getGreen(), (float) fxColor.getBlue(), (float) fxColor.getOpacity()));
                    } catch (Exception e) {
                        printError(e);
                        LOGGER.error(e.toString());
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                printError(e);
                LOGGER.error(e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Get history of querry
     */
    @FXML
    private void getHistoryDown() {
        clearError();
        if (historyLevel < 1) {
            historyLevel = -1;
            queryField.setText(tmpHistory);
        } else {
            historyLevel--;
            queryField.setText(formatQuery(history.get(historyLevel)));
        }
    }

    /**
     * Get history of querry
     */
    @FXML
    private void getHistoryUp() {
        clearError();
        if (historyLevel >= history.size() - 1) {
            queryField.setText(formatQuery(history.get(history.size() - 1)));
        } else {
            historyLevel++;
            queryField.setText(formatQuery(history.get(historyLevel)));
        }
    }


    @FXML
    private void updateTmpHistory() {
        clearError();
        tmpHistory = queryField.getText();
    }

    private String formatQuery(String query) {
        String newQuery = query;
        newQuery = newQuery.replaceAll(" WHERE ", "\nWHERE ");
        newQuery = newQuery.replaceAll(" AND ", "\nAND ");
        return newQuery;
    }

    @FXML
    private void saveResize() {
        double[] positions = mainPane.getDividerPositions();
        try {
            config.setDivisionPos(positions);
        } catch (IOException e) {
            printError(e);
            LOGGER.error(e.toString());
            e.printStackTrace();
        }
    }

    @FXML
    private void clearError() {
        errorLabel.setText("");
    }
}

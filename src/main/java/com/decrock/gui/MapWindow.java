package com.decrock.gui;

import com.decrock.app.Config;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import org.geotools.coverage.GridSampleDimension;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridFormatFinder;
import org.geotools.coverageio.gdal.ecw.ECWReader;
import org.geotools.data.DataSourceException;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.GeoTools;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.GridReaderLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.mbtiles.mosaic.MBTilesReader;
import org.geotools.renderer.GTRenderer;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.Stroke;
import org.geotools.styling.*;
import org.geotools.swing.JMapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;
import org.opengis.geometry.coordinate.LineString;
import org.opengis.parameter.GeneralParameterValue;
import org.opengis.parameter.ParameterValue;
import org.opengis.style.ContrastMethod;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.*;

/**
 * Handles the display of the map
 * Uses Geotools and GDAL for ecw. This part is a bit of a shitshow especially for the ecw that need GDAL for some
 * reason, but I didn't find a better alternative. It should probably be changed: use a newer version Java
 * and update Geotools and so on... For now the code is basically a remix of the geotools tutorial
 * http://docs.geotools.org/latest/userguide/tutorial/
 */
public class MapWindow {

    private static final Color LINE_COLOUR = Color.BLACK;
    private static final Color FILL_COLOUR = null;
    private static final float LINE_WIDTH = 1.0f;
    private static final Color SELECTED_COLOUR = null;
    private static final Color SELECTED_LINE_COLOUR = Color.RED;
    private static final float SELECTED_LINE_WIDTH = 3.0f;
    private static final float POINT_SIZE = 10.0f;
    private static float DEFAULT_OPACITY = 1f;
    private StyleFactory sf;
    private FilterFactory2 ff;
    private AbstractGridCoverage2DReader reader;
    private JMapPane mapPane;
    private SimpleFeatureSource featureSource;
    private String geometryAttributeName;
    private GeomType geometryType;
    private HashMap<File, Style> styles;
    private boolean panning = false;
    private Point panePos;
    private SimpleFeatureCollection selectedFeatures;
    private List<SimpleFeatureCollection> querySelectedFeatures;
    private MapContent map;
    private String activeAddress = "";
    private String activePlot = "";
    private List<Float> opacities;
    private int rasterCount = 0;
    private DecimalFormat df = new DecimalFormat("0000");
    private double rectX1, rectY1, rectX2, rectY2;
    private List<GridCoverage2D> gcList = new ArrayList<>();
    private Config config = Config.getInstance();
    private MapWindow() {
        querySelectedFeatures = new ArrayList<>();
        map = new MapContent();
        mapPane = new JMapPane(map);
        styles = new HashMap<>();
        sf = CommonFactoryFinder.getStyleFactory();
        ff = CommonFactoryFinder.getFilterFactory2();
    }

    public static MapWindow initMap() throws Exception {
        MapWindow mapWindow = new MapWindow();
        mapWindow.showMap(null, null);
        return mapWindow;
    }

    public JMapPane getMapPane() {
        return mapPane;
    }

    public String getActivePlot() {
        return activePlot;
    }

    public String getActiveAddress() {
        return activeAddress;
    }

    public boolean hasSelectedFeature() {
        return selectedFeatures != null && !selectedFeatures.isEmpty();
    }

    private void addRaster(File rasterFile) {
        Style rasterStyle;
        if (styles.containsKey(rasterFile)) {
            rasterStyle = styles.get(rasterFile);
        } else {
            AbstractGridFormat format = GridFormatFinder.findFormat(rasterFile);
            reader = format.getReader(rasterFile);
            rasterStyle = createRGBStyle(DEFAULT_OPACITY);
            styles.put(rasterFile, rasterStyle);
        }
        gcList.add(null);
        map.addLayer(new GridReaderLayer(reader, rasterStyle));
        rasterCount++;
        reader.dispose();
    }

    private void addShape(File shpFile) throws IOException {
        Style shpStyle;
        if (styles.containsKey(shpFile)) shpStyle = styles.get(shpFile);
        else {
            ShapefileDataStore store = new ShapefileDataStore(shpFile.toURL());
            featureSource = store.getFeatureSource();
            this.setGeometry();
            shpStyle = this.createDefaultStyle(DEFAULT_OPACITY);
            styles.put(shpFile, shpStyle);
        }
        Layer layer = new FeatureLayer(featureSource, shpStyle);
        map.addLayer(layer);
    }

    private void addECW(File ecwFile) throws DataSourceException {
        ECWReader ecwReader = new ECWReader(ecwFile);
        final ParameterValue<Boolean> jai =
                ((AbstractGridFormat) ecwReader.getFormat()).USE_JAI_IMAGEREAD.createValue();
        jai.setValue(true);
        try {


            GridCoverage2D gc = ecwReader.read(new GeneralParameterValue[]{jai});

            Style ecwStyle = createRGBStyle(DEFAULT_OPACITY, gc);
            map.addLayer(new GridReaderLayer(ecwReader, ecwStyle));
            gcList.add(gc);

        } catch (IOException e) {
            e.printStackTrace();
        }
//        reader.dispose();
    }

    private void addMBTile(File mbTileFile) throws IOException {
        MBTilesReader reader = new MBTilesReader(mbTileFile, null);
        GridCoverage2D cov;
        try {
            cov = reader.read(null);
            Style MBTileStyle = createRGBStyle(DEFAULT_OPACITY, cov);
            map.addLayer(new GridReaderLayer(reader, MBTileStyle));
        } catch (IOException giveUp) {
            throw new RuntimeException(giveUp);
        }
    }

    /**
     * Displays a GeoTIFF file overlaid with a Shapefile
     *
     * @param rasterFiles the GeoTIFF file
     * @param shpFiles    the Shapefile
     */
    public void showMap(List<File> rasterFiles, List<File> shpFiles) throws Exception {
        gcList.clear();
        featureSource = null;
        map.layers().clear();
        if (rasterFiles != null && !rasterFiles.isEmpty()) {
            for (File rasterFile : rasterFiles) {
                if (rasterFile.getName().endsWith(".mbtiles")) this.addMBTile(rasterFile);
                if (rasterFile.getName().endsWith(".ecw")) this.addECW(rasterFile);
                else this.addRaster(rasterFile);
            }
        }
        if (shpFiles != null && !shpFiles.isEmpty()) {
            for (File shpFile : shpFiles) {
                this.addShape(shpFile);
            }
        }
        if (reader != null) reader.dispose();
    }

    public void refresh() {
        mapPane.repaint();
    }

    public void zoomIn(MapMouseEvent ev) {
        zoom(ev.getWorldPos(), 0.9);
    }

    public void zoomOut(MapMouseEvent ev) {
        zoom(ev.getWorldPos(), 1.2);
    }

    void zoomOut() {
        double zoom = 1.4;
        this.zoom(zoom);
    }

    void zoomIn() {
        double zoom = 0.8;
        this.zoom(zoom);
    }

    private void zoom(double zoom) {
        Rectangle paneArea = mapPane.getVisibleRect();
        ReferencedEnvelope rec = mapPane.getDisplayArea();

        double scale = mapPane.getWorldToScreenTransform().getScaleX();
        double newScale = scale / zoom;
        double x = (rec.getMinX() + rec.getMaxX()) / 2d;
        double y = (rec.getMinY() + rec.getMaxY()) / 2d;
        DirectPosition2D mapPos = new DirectPosition2D(
                x - 0.5d * paneArea.getWidth() / newScale,
                y + 0.5d * paneArea.getHeight() / newScale);
        DirectPosition2D corner = new DirectPosition2D(
                x + 0.5d * paneArea.getWidth() / newScale,
                y - 0.5d * paneArea.getHeight() / newScale);
        Envelope2D newMapArea = new Envelope2D();
        newMapArea.setFrameFromDiagonal(mapPos, corner);
        mapPane.setDisplayArea(newMapArea);

//        System.out.println("*************");
//        System.out.println(mapPos);

    }

    private void zoom(DirectPosition2D mapPos, double zoom) {
        Rectangle paneArea = mapPane.getVisibleRect();
        double scale = mapPane.getWorldToScreenTransform().getScaleX();
        double newScale = scale / zoom;
        DirectPosition2D corner = new DirectPosition2D(
                mapPos.getX() + 0.5d * paneArea.getWidth() / newScale,
                mapPos.getY() - 0.5d * paneArea.getHeight() / newScale);

        Envelope2D newMapArea = new Envelope2D();
        newMapArea.setFrameFromCenter(mapPos, corner);
        mapPane.setDisplayArea(newMapArea);
    }

    public void resetZoom() {
        mapPane.setDisplayArea(map.getMaxBounds());
    }

    void setRectangleStart(MapMouseEvent ev) {
        DirectPosition2D mapPos = ev.getWorldPos();
        rectX1 = mapPos.getX();
        rectY1 = mapPos.getY();
    }

    void setRectangleEnd(MapMouseEvent ev) {
        DirectPosition2D mapPos = ev.getWorldPos();
        rectX2 = mapPos.getX();
        rectY2 = mapPos.getY();
    }

    void zoomByRectangle() {
        if (rectX1 > rectX2) {
            double temp = rectX1;
            rectX1 = rectX2;
            rectX2 = temp;
        }
        if (rectY1 > rectY2) {
            double temp = rectY1;
            rectY1 = rectY2;
            rectY2 = temp;
        }
        if (rectX2 - rectX1 < config.getMaxZoom()) rectX2 = rectX1 + config.getMaxZoom();
        if (rectY2 - rectY1 < config.getMaxZoom()) rectY2 = rectY1 + config.getMaxZoom();
        DirectPosition2D corner1 = new DirectPosition2D(rectX1, rectY1);
        DirectPosition2D corner2 = new DirectPosition2D(rectX2, rectY2);
        Envelope2D newMapArea = new Envelope2D();
        newMapArea.setFrameFromDiagonal(corner1, corner2);
        mapPane.setDisplayArea(newMapArea);

    }

    void startPanning(MapMouseEvent ev) {
        panePos = ev.getPoint();
        panning = true;
    }

    void endPanning(MapMouseEvent ev) {
        panning = false;
    }

    void pan(MapMouseEvent ev) {
        if (panning) {
            Point pos = ev.getPoint();
            if (!pos.equals(panePos)) {
                mapPane.moveImage(pos.x - panePos.x, pos.y - panePos.y);
                panePos = pos;
            }
        }
    }

    /**
     * Set the opacity of a given layer to the given value
     *
     * @param indexLayer index of the layer
     * @param opacity    target opacity
     */
    void setLayerOpacity(int indexLayer, float opacity) {
        map.layers().get(indexLayer).getStyle();
        Style style;
        if (indexLayer <= rasterCount) {
            if (gcList.get(indexLayer) == null) style = createRGBStyle(opacity);
            else style = createRGBStyle(opacity, gcList.get(indexLayer));
            if (style != null) {
                Layer layer = map.layers().get(indexLayer);

                ((GridReaderLayer) layer).setStyle(style);
            }
        } else {
            style = createDefaultStyle(opacity);
            if (style != null) {
                Layer layer = map.layers().get(indexLayer);
                ((FeatureLayer) layer).setStyle(style);
            }
        }
    }

    void setLayerVisible(int indexLayer, boolean visible) {
        map.layers().get(indexLayer).setVisible(visible);
    }

    /**
     * This method examines the names of the sample dimensions in the provided coverage looking for
     * "red...", "green..." and "blue..." (case insensitive match). If these names are not found
     * it uses bands 1, 2, and 3 for the red, green and blue channels. It then sets up a raster
     * symbolizer and returns this wrapped in a Style.
     *
     * @return a new Style object containing a raster symbolizer set up for RGB image
     */
    private Style createRGBStyle(float opacity) {
        GridCoverage2D cov = null;
        try {
            cov = reader.read(null);
        } catch (IOException giveUp) {
            throw new RuntimeException(giveUp);
        }

        return createRGBStyle(opacity, cov);
    }

    private Style createRGBStyle(float opacity, GridCoverage2D cov) {
        // We need at least three bands to create an RGB style
        int numBands = cov.getNumSampleDimensions();
        if (numBands < 3) {
            return null;
        }
        // Get the names of the bands
        String[] sampleDimensionNames = new String[numBands];
        for (int i = 0; i < numBands; i++) {
            GridSampleDimension dim = cov.getSampleDimension(i);
            sampleDimensionNames[i] = dim.getDescription().toString();
        }
        final int RED = 0, GREEN = 1, BLUE = 2;
        int[] channelNum = {-1, -1, -1};
        // We examine the band names looking for "red...", "green...", "blue...".
        // Note that the channel numbers we record are indexed from 1, not 0.
        for (int i = 0; i < numBands; i++) {
            String name = sampleDimensionNames[i].toLowerCase();
            if (name != null) {
                if (name.matches("red.*")) {
                    channelNum[RED] = i + 1;
                } else if (name.matches("green.*")) {
                    channelNum[GREEN] = i + 1;
                } else if (name.matches("blue.*")) {
                    channelNum[BLUE] = i + 1;
                }
            }
        }
        // If we didn't find named bands "red...", "green...", "blue..."
        // we fall back to using the first three bands in order
        if (channelNum[RED] < 0 || channelNum[GREEN] < 0 || channelNum[BLUE] < 0) {
            channelNum[RED] = 1;
            channelNum[GREEN] = 2;
            channelNum[BLUE] = 3;
        }
        // Now we create a RasterSymbolizer using the selected channels
        SelectedChannelType[] sct = new SelectedChannelType[cov.getNumSampleDimensions()];
        ContrastEnhancement ce = sf.contrastEnhancement(ff.literal(opacity), ContrastMethod.NORMALIZE);
        for (int i = 0; i < 3; i++) {
            sct[i] = sf.createSelectedChannelType(String.valueOf(channelNum[i]), ce);
        }
        RasterSymbolizer sym = sf.getDefaultRasterSymbolizer();
        sym.setOpacity(CommonFactoryFinder.getFilterFactory(GeoTools.getDefaultHints()).literal(opacity));
        ChannelSelection sel = sf.channelSelection(sct[RED], sct[GREEN], sct[BLUE]);
        sym.setChannelSelection(sel);

        return SLD.wrapSymbolizers(sym);

    }

    private Style createDefaultStyle(float opacity) {
        Rule rule = createRule(LINE_COLOUR, FILL_COLOUR, LINE_WIDTH, opacity);
        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(rule);
        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    private Style createSelectedStyle(Set<FeatureId> IDs, float opacity) {
        return createStyle(IDs, SELECTED_LINE_COLOUR, SELECTED_LINE_WIDTH, FILL_COLOUR, opacity);
    }

    private Style createStyle(Set<FeatureId> IDs, Color lineColor, float lineWidth, Color fillColor, float opacity) {
        Rule selectedRule = createRule(lineColor, fillColor, lineWidth, opacity);
        selectedRule.setFilter(ff.id(IDs));
        Rule otherRule = createRule(LINE_COLOUR, FILL_COLOUR, LINE_WIDTH, opacity);
        otherRule.setElseFilter(true);
        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(selectedRule);
        fts.rules().add(otherRule);
        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    private Rule createRule(Color outlineColor, Color fillColor, double lineWidth, float opacity) {
        Symbolizer symbolizer = null;
        Fill fill = null;
        Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(lineWidth));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(opacity));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(opacity));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
        }

        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }

    private void setGeometry() {
        GeometryDescriptor geomDesc = featureSource.getSchema().getGeometryDescriptor();
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz) ||
                MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeomType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz) ||
                MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeomType.LINE;

        } else {
            geometryType = GeomType.POINT;
        }
    }

    void getMousePos(MapMouseEvent ev) {
        System.out.println(ev);
        System.out.println(ev.getWorldPos());
    }

    void selectFeatures(MapMouseEvent ev) throws IOException {
        if (featureSource == null) return;
        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        Point screenPos = ev.getPoint();
        Rectangle screenRect = new Rectangle(screenPos.x, screenPos.y, 2, 2);
        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = mapPane.getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                map.getCoordinateReferenceSystem());
        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(bbox));
        /*
         * Use the filter to identify the selected features
         */
        SimpleFeatureCollection tmpSelectedFeatures = featureSource.getFeatures(filter);
        if (tmpSelectedFeatures.equals(featureSource)) return;

        selectedFeatures = featureSource.getFeatures(filter);
        Set<FeatureId> IDs = new HashSet<>();
        activeAddress = "";
        activePlot = "";

        SimpleFeatureIterator iter = selectedFeatures.features();
        SimpleFeature selectedFeature;
        if (iter.hasNext()) {
            selectedFeature = iter.next();
            activeAddress = String.valueOf(selectedFeature.getAttribute("ADRESSE"));
            activePlot = selectedFeature.getAttribute("CCOSEC") + df.format(selectedFeature.getAttribute("PARCELLE"));
            IDs.add(selectedFeature.getIdentifier());
        } else {
            selectedFeature = null;
        }
        displaySelectedFeatures(IDs);
    }

    public void selectPlots(List<String> plotsList, Color color) throws Exception {
        Set<FeatureId> IDs = new HashSet<FeatureId>();
        for (String sheet : plotsList) {
            Filter filter = CQL.toFilter("IDENT = '" + sheet.substring(0, 6) + "'");
            querySelectedFeatures.add(featureSource.getFeatures(filter));
            SimpleFeatureIterator iter = querySelectedFeatures.get(querySelectedFeatures.size() - 1).features();
            while (iter.hasNext()) {
                SimpleFeature feature = iter.next();
                IDs.add(feature.getIdentifier());
            }
        }
        displayQueryFeatures(IDs, color);
    }

    private void displaySelectedFeatures(Set<FeatureId> IDs) {
        if (map.layers().size() - rasterCount == 0) return;


        Style style;
        if (IDs.isEmpty()) {
            style = createDefaultStyle(DEFAULT_OPACITY);
        } else {
            style = createSelectedStyle(IDs, DEFAULT_OPACITY);
        }
        FeatureLayer layer = (FeatureLayer) map.layers().get(map.layers().size() - 1);
        layer.setStyle(style);
    }

    private void displayQueryFeatures(Set<FeatureId> IDs, Color color) {
        if (map.layers().size() - rasterCount == 0) return;
        Style style;
        if (IDs.isEmpty()) {
            style = createDefaultStyle(DEFAULT_OPACITY);
        } else {
            style = createStyle(IDs, LINE_COLOUR, LINE_WIDTH, color, DEFAULT_OPACITY);
        }
        Layer layer = map.layers().get(map.layers().size() - 1);
        ((FeatureLayer) layer).setStyle(style);
    }

    public void saveImage(final String file, final int imageWidth, final String extension) {

        GTRenderer renderer = new StreamingRenderer();
        renderer.setMapContent(map);

        Rectangle imageBounds = null;
        ReferencedEnvelope mapBounds = null;
        try {
            mapBounds = map.getMaxBounds();
            double heightToWidth = mapBounds.getSpan(1) / mapBounds.getSpan(0);
            imageBounds = new Rectangle(
                    0, 0, imageWidth, (int) Math.round(imageWidth * heightToWidth));
        } catch (Exception e) {
            // failed to access map layers
            throw new RuntimeException(e);
        }

        BufferedImage image = new BufferedImage(imageBounds.width, imageBounds.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D gr = image.createGraphics();
        gr.setPaint(Color.WHITE);
        gr.fill(imageBounds);
        try {
            renderer.paint(gr, imageBounds, mapBounds);
            File fileToSave = new File(file);
            ImageIO.write(image, extension, fileToSave);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void dispose() {
        map.dispose();
    }

    private enum GeomType {POINT, LINE, POLYGON}
}

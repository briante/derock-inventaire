package com.decrock.gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 * Controller for the map part of the GUI
 */
public class MapLayerController {

    public static void getMapLayerController(final MapWindow mapWindow, FlowPane pane, String name, final int num, boolean addOpacitySlider) {
        VBox vBox = new VBox();
        CheckBox visibility = new CheckBox(truncate(name, 15));
        visibility.setSelected(true);
        visibility.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                mapWindow.setLayerVisible(num, newValue);
            }
        });
        vBox.getChildren().add(visibility);
        if (addOpacitySlider) {
            Slider opacitySlider = new Slider(0, 1, 1);
            opacitySlider.setOnMouseReleased((event -> {
                System.out.println(opacitySlider.getValue());
                mapWindow.setLayerOpacity(num, (float) opacitySlider.getValue());
            }));
//            opacitySlider.valueProperty().addListener(new ChangeListener<Number>() {
//                public void changed(ObservableValue<? extends Number> ov,
//                                    Number old_val, Number new_val) {
//                    mapWindow.setLayerOpacity(num, new_val.floatValue());
//                }
//            });
            vBox.getChildren().add(opacitySlider);
        }
        vBox.setAlignment(Pos.TOP_CENTER);
        pane.getChildren().add(vBox);
    }

    private static String truncate(String s, int length) {
        if (s.length() <= length) return s;
        else return (s.substring(0, length - 3) + "...");
    }
}

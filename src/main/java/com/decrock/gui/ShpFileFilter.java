package com.decrock.gui;

import java.io.File;
import java.io.FileFilter;

public class ShpFileFilter implements FileFilter {

    private final String[] okFileExtensions = new String[]{"shp"};

    public boolean accept(File file) {
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}

package com.decrock.gui;

import com.decrock.app.Config;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Creates the display of a sheet from the database and the configuration (customized by the client)
 * Handles data writing and retrieving with the GUI
 */
public class Sheet {

    private static Config config = Config.getInstance();
    private int id;
    private Map<String, Node> sheet;
    private Map<String, String> equivalencesToValue;
    private Map<String, String> equivalencesToLabel;

    private Sheet() {
        this.id = 0;
        this.sheet = new HashMap<String, Node>();
        this.equivalencesToValue = new HashMap<String, String>();
        this.equivalencesToLabel = new HashMap<String, String>();

    }

    public static Sheet buildSheet(Pane container, boolean modifiable) {
        Sheet sheet = new Sheet();
        FlowPane content = sheet.build(modifiable);
        container.getChildren().add(content);
        return sheet;
    }

    public static Sheet buildSheet(ScrollPane container, boolean modifiable) {
        Sheet sheet = new Sheet();
        FlowPane content = sheet.build(modifiable);
        container.setContent(content);
        return sheet;
    }

    public int getId() {
        return id;
    }

    private FlowPane build(boolean modifiable) {
        FlowPane content = new FlowPane();
        content.setPadding(new Insets(5));
        content.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        content.setVgap(10);
        content.setHgap(10);
        content.setMouseTransparent(!modifiable);
        for (String field : config.getFieldList()) {
            if (!field.equals(config.getId_fiche())) content.getChildren().add(this.buildField(field));
        }
        return content;
//        container.getChildren().add(content);
    }

    private VBox buildField(String field) {
        VBox vbox = new VBox(new Label(field));
        vbox.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        vbox.setSpacing(5);
        String type = config.getType(field);
        if (type != null) {
            switch (type) {
                case "textfield":
                    TextField textField = this.buildTextField(field);
                    this.setStyle(textField, field);
                    vbox.getChildren().add(textField);
                    sheet.put(field, textField);
                    break;
                case "textarea":
                    TextArea textArea = this.buildTextArea(field);
                    textArea.setWrapText(true);
                    textArea.setPrefSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
                    Text textHolder = new Text();
                    textHolder.textProperty().bind(textArea.textProperty());
                    textHolder.layoutBoundsProperty().addListener(new ChangeListener<Bounds>() {
                        @Override
                        public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
                            newValue.getHeight();
                            textArea.setMinHeight(textHolder.getLayoutBounds().getHeight() + 20); // +20 is for paddings

                        }
                    });
                    vbox.getChildren().add(textArea);
                    this.setStyle(textArea, field);
                    sheet.put(field, textArea);
                    break;
                case "choicebox":
                    ChoiceBox choiceBox = this.buildChoiceBox(field, config.getChoices(field));
                    this.setStyle(choiceBox, field);
                    vbox.getChildren().add(choiceBox);
                    sheet.put(field, choiceBox);
                    break;
                case "checkboxlist":
                    FlowPane flowPane = this.buildCheckBoxList(field, config.getChoices(field));
                    this.setStyle(flowPane, field);
                    vbox.getChildren().add(flowPane);
                    sheet.put(field, flowPane);
                    break;
                case "checkbox":
                    CheckBox checkBox = this.buildCheckBox(field);
                    vbox.getChildren().add(checkBox);
                    sheet.put(field, checkBox);
                    break;
            }
        }
        return vbox;
    }

    private TextField buildTextField(String name) {
        return buildTextField(name, "");
    }

    private TextField buildTextField(String name, String defaultValue) {
        TextField txtfield = new TextField();
        txtfield.setPromptText(name);
        txtfield.setText((defaultValue != null) ? defaultValue : "");
        return txtfield;
    }

    private TextArea buildTextArea(String name) {
        return buildTextArea(name, "");
    }

    private TextArea buildTextArea(String name, String defaultValue) {
        TextArea txtarea = new TextArea();
        txtarea.setPromptText(name);
        txtarea.setText((defaultValue != null) ? defaultValue : "");
        return txtarea;
    }

    private List<String> formatChoiceList(List<Map<String, String>> choicesList) {
        List<String> choicesList2 = new ArrayList<String>(choicesList.size());
        for (Map<String, String> choice : choicesList) {
            String label;
            if (choice.containsKey("label")) {
                label = choice.get("label");
            } else {
                label = choice.get("valeur");
            }
            equivalencesToValue.put(label, choice.get("valeur"));
            equivalencesToLabel.put(choice.get("valeur"), label);
            choicesList2.add(label);
        }
        return choicesList2;
    }

    private ChoiceBox<String> buildChoiceBox(String name, List<Map<String, String>> choicesList) {
        ObservableList<Object> choices = FXCollections.observableArrayList();
        choices.addAll(formatChoiceList(choicesList));
        ChoiceBox choiceBox = new ChoiceBox(choices);
        return choiceBox;
    }

    private FlowPane buildCheckBoxList(String name, List<Map<String, String>> choicesList) {
        FlowPane checkBoxPane = new FlowPane();
        checkBoxPane.setHgap(5);
        checkBoxPane.setVgap(5);
        List<String> choicesList2 = formatChoiceList(choicesList);
        for (String label : choicesList2) {
            CheckBox checkbox = new CheckBox();
            checkbox.setText(label);
            checkBoxPane.getChildren().add(checkbox);
        }
        return checkBoxPane;
    }

    private CheckBox buildCheckBox(String name) {
        CheckBox checkbox = new CheckBox();
        checkbox.setAllowIndeterminate(true);
        return checkbox;
    }

    public void setDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        ((TextField) sheet.get(config.getDate_fiche())).setText(dateFormat.format(date));
    }

    public Map<String, String> getSheetContent() {
        Map<String, String> h = new HashMap<String, String>();
        h.put(config.getId_fiche(), String.valueOf(this.id));
        for (String field : config.getFieldList()) {
            if (!field.equals(config.getId_fiche()) && sheet.containsKey(field)) {
                Node node = sheet.get(field);
                String type = config.getType(field);
                String value = "";
                if (type != null) {
                    switch (type) {
                        case "textfield":
                            value = ((TextField) node).getText();
                            break;
                        case "textarea":
                            value = ((TextArea) node).getText();
                            break;
                        case "choicebox":
                            value = equivalencesToValue.get(((ChoiceBox<String>) node).getValue());
                            break;
                        case "checkboxlist":
                            FlowPane checkBoxPane = (FlowPane) node;
                            for (Node checkbox : checkBoxPane.getChildren())
                                value += ((((CheckBox) checkbox).isSelected()) ? " ; "
                                        + equivalencesToValue.get(((CheckBox) checkbox).getText()) : "");
                            if (value.length() > 1) value = value.substring(3);
                            break;
                        case "checkbox":
                            CheckBox cb = (CheckBox) node;
                            value = (cb.isIndeterminate() ? "I" : (cb.isSelected() ? "O" : "N"));
                            break;
                    }
                }
                h.put(field, value == null ? "" : String.valueOf(value));
            }

        }
        return h;
    }

    public void setSheetContent(Map<String, String> data) {
        try {
            this.id = Integer.valueOf(data.get(config.getId_fiche()));
        } catch (NumberFormatException e) {
            this.id = 0;
        }

        for (String field : config.getFieldList()) {
            if (sheet.containsKey(field) && data.get(field) != null && !data.get(field).equals("")) {
                String type = config.getType(field);
                if (type != null) {
                    switch (type) {
                        case "textfield":
                            ((TextField) this.sheet.get(field)).setText(data.get(field));
                            break;
                        case "textarea":
                            ((TextArea) this.sheet.get(field)).setText(data.get(field));
                            break;
                        case "choicebox":
                            ((ChoiceBox<String>) this.sheet.get(field)).setValue(data.get(equivalencesToLabel.get(field)));
                            break;
                        case "checkboxlist":
                            String[] list = data.get(field).split(" ; ");
                            FlowPane checkBoxPane = (FlowPane) this.sheet.get(field);
                            search:
                            for (String text : list) {
                                for (Node child : checkBoxPane.getChildren()) {
                                    CheckBox checkBox = (CheckBox) child;
                                    if (checkBox.getText().equals(equivalencesToLabel.get(text))) {
                                        checkBox.setSelected(true);
                                        continue search;
                                    }
                                }
                            }
                            break;
                        case "checkbox":
                            CheckBox cb = (CheckBox) this.sheet.get(field);
                            cb.setIndeterminate(sheet.get(field).equals("I"));
                            cb.setSelected(sheet.get(field).equals("O"));
                            break;
                    }
                }
            }

        }
    }

    public void setStyle(Node node, String field) {
        String style = "";
        String width = config.getWidth(field);
        String height = config.getHeight(field);

        if (height != null) {
            style += String.format("-fx-pref-height: %spx;\n", config.getFieldsProp().get(field).get("hauteur"));
        }
        if (width != null) {
            style += String.format("-fx-pref-width: %spx;\n", config.getFieldsProp().get(field).get("largeur"));
        }
        if (style.length() > 0) {
            node.setStyle(style);
        }
    }
}

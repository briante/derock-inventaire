package com.decrock.app;

/**
 * Useless attempt to separate the setting that should be modified by the client and the ones they should not touch
 */
public class Settings {
    private String documentation_path;
    private String photos_path;
    private String raster_path;
    private String shp_path;
    private String db_path;
    private double division1;
    private double division2;
    private String log_level;

    public String getDocumentation_path() {
        return documentation_path;
    }

    public String getPhotos_path() {
        return photos_path;
    }

    public String getRaster_path() {
        return raster_path;
    }

    public String getShp_path() {
        return shp_path;
    }

    public String getDb_path() {
        return db_path;
    }

    public double getDivision1() {
        return division1;
    }

    public double getDivision2() {
        return division2;
    }
}

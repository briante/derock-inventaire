package com.decrock.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles the configuration reading and writing
 */
public class Config {

    private static Config INSTANCE = null;
    private ConfigMap configMap;
    private Map<String, Map<String, Object>> fieldsProps;
    private Map<String, Integer> dbStructure;
    private List<String> fieldList;
    private Map<String, String> dbDetailStructure;


    private Config() {
        this.fieldsProps = new HashMap<>();
    }

    public static Config getInstance() {
        return Config.INSTANCE;
    }

    static void initConfig(File configFile) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        Config.INSTANCE = new Config();

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile), "UTF-8"));
        Config.INSTANCE.configMap = mapper.readValue(reader, ConfigMap.class);
        for (Map<String, Object> field : Config.INSTANCE.configMap.getFiche()) {
            Config.INSTANCE.fieldsProps.put((String) field.get("nom"), field);
        }
    }

    public Map<String, Map<String, Object>> getFieldsProp() {
        return this.fieldsProps;
    }

    public String getNom() {
        return Config.INSTANCE.configMap.getNom();
    }

    public String getLog_level() {
        return Config.INSTANCE.configMap.getLog_level();
    }

    public List<Map<String, Object>> getFiche() {
        return Config.INSTANCE.configMap.getFiche();
    }

    public String getMainTableName() {
        return Config.INSTANCE.configMap.getNom_table_principale();
    }

    public String getModifTableName() {
        return Config.INSTANCE.configMap.getNom_table_modif();
    }

    public String getDocumentation_path() {
        return Config.INSTANCE.configMap.getDocumentation_path();
    }

    public String getPhotos_path() {
        return Config.INSTANCE.configMap.getPhotos_path();
    }

    public void setPhotos_path(String path) throws IOException {
        Config.INSTANCE.configMap.setPhotos_path(path);
        path = path.replaceAll("\\\\", "/");
        updateParameter("photos_path", path);
    }

    public String getRaster_path() {
        return Config.INSTANCE.configMap.getRaster_path();
    }

    public void setRaster_path(String path) throws IOException {
        Config.INSTANCE.configMap.setRaster_path(path);
        path = path.replaceAll("\\\\", "/");
        updateParameter("raster_path", path);
    }

    public String getShp_path() {
        return Config.INSTANCE.configMap.getShp_path();
    }

    public void setShp_path(String path) throws IOException {
        Config.INSTANCE.configMap.setShp_path(path);
        path = path.replaceAll("\\\\", "/");
        updateParameter("shp_path", path);
    }

    public String getDb_path() {
        return Config.INSTANCE.configMap.getDb_path();
    }

    public void setDb_path(String path) throws IOException {
        Config.INSTANCE.configMap.setDb_path(path);
        path = path.replaceAll("\\\\", "/");
        updateParameter("db_path", path);
    }

    public String getId_fiche() {
        return Config.INSTANCE.configMap.getId_fiche();
    }

    public String getReference_fiche() {
        return Config.INSTANCE.configMap.getReference_fiche();
    }

    public String getAdresse_fiche() {
        return Config.INSTANCE.configMap.getAdresse_fiche();
    }

    public String getEnqueteur_fiche() {
        return Config.INSTANCE.configMap.getEnqueteur_fiche();
    }

    public String getDate_fiche() {
        return Config.INSTANCE.configMap.getDate_fiche();
    }

    public int getPreview_size() {
        return Config.INSTANCE.configMap.getPreview_size();
    }

    public String getCode_insee() {
        return Config.INSTANCE.configMap.getCode_insee();
    }

    public double getMaxZoom() {
        return Config.INSTANCE.configMap.getMaxZoom();
    }

    public List<String> getMots_ignores() {
        return Config.INSTANCE.configMap.getMots_ignores();
    }

    public void setDocumentationPath(String path) throws IOException {
        Config.INSTANCE.configMap.setDocumentation_path(path);
        path = path.replaceAll("\\\\", "/");
        updateParameter("documentation_path", path);
    }

    public void setDivisionPos(double[] values) throws IOException {
    }

    private void updateParameter(String param, String value) throws IOException {
        // input the file content to the StringBuffer "input"
        BufferedReader file = new BufferedReader(new FileReader(App.configFile.getPath()));
        String line;
        StringBuffer inputBuffer = new StringBuffer();

        while ((line = file.readLine()) != null) {
            inputBuffer.append(line);
            inputBuffer.append('\n');
        }
        String inputStr = inputBuffer.toString();
        file.close();
        String regex = "(?=" + param + ":).*";
        inputStr = inputStr.replaceAll(regex, param + ": " + value);

        // write the new String with the replaced line OVER the same file
        FileWriter fw = new FileWriter(App.configFile.getPath());
        fw.write(inputStr);
        fw.close();
    }

    /**
     * Get all the database fields, returns their name and the code of their type
     * @return
     */
    public Map<String, Integer> getDbStructure() {
        return dbStructure;
    }

    public void setDbStructure(Map<String, Integer> dbStructure) {
        this.dbStructure = dbStructure;
    }

    /**
     * Returns the type of display of a field, if it is written in the config.yaml file, else the default is a textfield
     * @param field
     * @return
     */
    public String getType(String field) {
        if (fieldsProps.containsKey(field) && fieldsProps.get(field).containsKey("type")) {
            return (String) fieldsProps.get(field).get("type");
        } else {
            return "textfield";
        }
    }

    /**
     * Returns the choices of a field if it is listed in the config.yaml file
     * @param field
     * @return
     */
    public List<Map<String, String>> getChoices(String field) {
        if (fieldsProps.containsKey(field) && fieldsProps.get(field).containsKey("choix")) {
            return (List<Map<String, String>>) fieldsProps.get(field).get("choix");
        } else {
            return null;
        }
    }

    /**
     * Returns the preferred height of a field if it is listed in the config.yaml file
     * @param field
     * @return
     */
    public String getHeight(String field) {
        if (fieldsProps.containsKey(field) && fieldsProps.get(field).containsKey("hauteur"))
            return String.valueOf(fieldsProps.get(field).get("hauteur"));
        else return null;
    }

    /**
     * Returns the preferred width of a field if it is listed in the config.yaml file
     * @param field
     * @return
     */
    public String getWidth(String field) {
        if (fieldsProps.containsKey(field) && fieldsProps.get(field).containsKey("largeur"))
            return String.valueOf(getFieldsProp().get(field).get("largeur"));
        else return null;
    }

    public List<String> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<String> fieldList) {
        this.fieldList = fieldList;
    }

    public Map<String, String> getDbDetailStructure() {
        return dbDetailStructure;
    }

    public void setDbDetailStructure(Map<String, String> dbDetailStructure) {
        this.dbDetailStructure = dbDetailStructure;
    }
}

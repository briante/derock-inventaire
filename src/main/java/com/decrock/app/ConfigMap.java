package com.decrock.app;

import java.util.List;
import java.util.Map;

/**
 * Map for YAML reading of the configuration file
 */
public class ConfigMap {

    private String nom;
    private String log_level;
    private List<Map<String, Object>> fiche;
    private String nom_table_principale;
    private String nom_table_modif;
    private String documentation_path;
    private String photos_path;
    private String raster_path;
    private String shp_path;
    private String db_path;
    private String id_fiche;
    private String reference_fiche;
    private String adresse_fiche;
    private String enqueteur_fiche;
    private String date_fiche;
    private int preview_size;
    private String code_insee;
    private double maxZoom;
    private List<String> mots_ignores;

    public String getNom() {
        return nom;
    }

    public List<Map<String, Object>> getFiche() {
        return fiche;
    }

    public String getNom_table_principale() {
        return nom_table_principale;
    }

    public String getLog_level() {
        return log_level;
    }

    public String getDocumentation_path() {
        return documentation_path;
    }

    public void setDocumentation_path(String documentation_path) {
        this.documentation_path = documentation_path;
    }

    public String getPhotos_path() {
        return photos_path;
    }

    public void setPhotos_path(String photos_path) {
        this.photos_path = photos_path;
    }

    public String getRaster_path() {
        return raster_path;
    }

    public void setRaster_path(String raster_path) {
        this.raster_path = raster_path;
    }

    public String getShp_path() {
        return shp_path;
    }

    public void setShp_path(String shp_path) {
        this.shp_path = shp_path;
    }

    public String getDb_path() {
        return db_path;
    }

    public void setDb_path(String db_path) {
        this.db_path = db_path;
    }

    public String getId_fiche() {
        return id_fiche;
    }

    public String getReference_fiche() {
        return reference_fiche;
    }

    public String getAdresse_fiche() {
        return adresse_fiche;
    }

    public String getEnqueteur_fiche() {
        return enqueteur_fiche;
    }

    public String getDate_fiche() {
        return date_fiche;
    }

    public int getPreview_size() {
        return preview_size;
    }

    public String getCode_insee() {
        return code_insee;
    }

    public double getMaxZoom() {
        return maxZoom;
    }

    public List<String> getMots_ignores() {
        return mots_ignores;
    }

    public String getNom_table_modif() {
        return nom_table_modif;
    }
}

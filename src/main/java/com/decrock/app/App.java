package com.decrock.app;

import com.decrock.gui.History;
import com.decrock.gui.PDFViewer;
import it.geosolutions.imageio.gdalframework.GDALUtilities;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.apache.log4j.Logger;
import org.geotools.coverageio.gdal.ecw.ECWFormatFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class App extends Application {

    private static final int SPLASH_WIDTH = 200;
    private static final int SPLASH_HEIGHT = 75;
    public static File file = new File(".");
    public static File configFile;
    private static Image APPLICATION_ICON;
    private final Logger LOGGER = Logger.getLogger("app");
    private boolean maximize = true;
    private boolean resizable = true;
    private Pane splashLayout;
    private ProgressBar loadProgress;
    private Label progressText;
    private Stage mainStage;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Initialize the application
     */
    @Override
    public void init() {
        try {
            APPLICATION_ICON = new Image(getClass().getResourceAsStream("/gui/icon.png"));
            Image SPLASH_IMAGE = new Image(getClass().getResourceAsStream("/gui/splash.png"));

            History.historyFile = new File(getClass().getResource("/config/.history").getFile());
            try {
                History.getHistory();
            } catch (FileNotFoundException e) {
                History.historyFile = new File("conf/config/.history");
            }
            ImageView splash = new ImageView(SPLASH_IMAGE);
            loadProgress = new ProgressBar();
            loadProgress.setPrefWidth(SPLASH_WIDTH * 3);
            progressText = new Label("  Chargement . . .");
            progressText.setFont(new Font(12));
            progressText.setWrapText(true);
            splashLayout = new VBox();
            splashLayout.getChildren().addAll(splash, loadProgress, progressText);
            progressText.setAlignment(Pos.CENTER);
            splashLayout.setStyle(
                    "-fx-padding: 0;"
            );
        } catch (Exception e) {
            LOGGER.error(e.toString());
            e.printStackTrace();
            showError(e);
        }
    }

    /**
     * Performs the loading of the application, this part is done while the splash screen is displayed
     * @param initStage
     * @throws Exception
     */
    @Override
    public void start(final Stage initStage) throws Exception {
        final Task<ObservableList<String>> loadingTask = new Task<ObservableList<String>>() {
            @Override
            protected ObservableList<String> call() throws IOException, InterruptedException {

                // Configuration loading
                updateMessage("  Chargement de la configuration . . .");

                configFile = new File((getClass().getResource("/config/config.yaml")).getFile());
                if (!configFile.exists()) {
                    configFile = new File("conf/config/config.yaml");
                }

                try {
                    Config.initConfig(configFile);
                } catch (Exception e) {
                    updateMessage("  Erreur de chargement de la configuration : " + e.toString());
                    e.printStackTrace();
                    Thread.sleep(3000);
                    stop();
                }

//                // Client warranty
//                try {
//                    if (new Date().after(new SimpleDateFormat("dd/MM/yyyy").parse("31/08/2020"))){
//                        updateMessage("  Erreur de chargement de la configuration");
//                        Thread.sleep(3000);
//                        stop();
//                    }
//                } catch (ParseException e) {
//                    updateMessage("  Erreur de chargement de la configuration : " + e.toString());
//                    e.printStackTrace();
//                }

                // Load GDAL
                GDALUtilities.loadGDAL();
                updateMessage("  GDAL ecw module : " + (GDALUtilities.isDriverAvailable("ECW") ? "OK" : "Indisponible"));
                Thread.sleep(1000);
                updateMessage("  Generation des aperçus pdf . . .");

                // Check and create the preview of the pdf files
                List<File> fileList = PDFViewer.getAllPDFFiles(new File(Config.getInstance().getDocumentation_path() + "/doc"));
                int fileCount = fileList.size();

                if (fileCount > 0) {
                    // Create the previews for the pdf, only done once
//                    PDFViewer.previewURI = getClass().getResource("/pdf_preview/").getPath();
//                    if (PDFViewer.previewURI  == null) PDFViewer.previewURI  = "conf/pdf_preview/";
                    PDFViewer.previewURI = "conf/pdf_preview/";
                    LOGGER.info("Dossier PDF: " + PDFViewer.previewURI);
                    File pdfDir = new File(PDFViewer.previewURI);
                    if (!pdfDir.exists()) {
                        PDFViewer.previewURI = "conf/pdf_preview/";
                        pdfDir = new File(PDFViewer.previewURI);
                    }
                    if (pdfDir.mkdir()) {
                        System.out.println("PDF Folder created");
                    }
                    int fileNum = 0;
                    for (File f : fileList) {
                        updateMessage(String.format("  Generation des preview pdf %s / %s", fileNum, fileCount));
                        PDFViewer.makePreview(f);
                        fileNum++;
                    }
                }
                return null;
            }
        };

        showSplash(
                initStage,
                loadingTask,
                () -> showMainStage()
        );
        new Thread(loadingTask).start();
    }

    /**
     * Display the application main window by loading the fxml (Uses JavaFX for GUI)
     */
    private void showMainStage() {
        Scene scene;
        try {
            URL fxmlURL = getClass().getResource("/gui/mainWindow.fxml");
            System.out.println(fxmlURL);
            FXMLLoader loader = new FXMLLoader(fxmlURL);
            Parent rootLayout = loader.load();
            scene = new Scene(rootLayout);
            mainStage = new Stage();
            mainStage.getIcons().add(APPLICATION_ICON);
            mainStage.setTitle(Config.getInstance().getNom());
            mainStage.setMaximized(maximize);
            mainStage.setResizable(resizable);
            mainStage.setScene(scene);
            mainStage.show();
        } catch (Exception e) {
            LOGGER.error(e.toString());
            e.printStackTrace();
            showError(e);
        }

    }

    /**
     * Error display for when an error occurs during launch
     * @param e
     */
    private void showError(Exception e) {
        Label error = new Label("L'application n'a pas pu démarrer");
        error.setFont(Font.font("System", FontWeight.BOLD, 15));
        Label errorMsg = new Label(e.getMessage());
        VBox main = new VBox(error, errorMsg);
        main.setAlignment(Pos.TOP_CENTER);
        main.setPadding(new Insets(20));
        main.setSpacing(10);
        Scene scene = new Scene(main);
        maximize = false;
        resizable = false;
        mainStage = new Stage();
        mainStage.getIcons().add(APPLICATION_ICON);
        mainStage.setTitle(Config.getInstance().getNom());
        mainStage.setMaximized(maximize);
        mainStage.setResizable(resizable);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Display the splash during the loading of the application
     * @param initStage
     * @param task
     * @param initCompletionHandler
     */
    private void showSplash(
            final Stage initStage,
            Task<?> task,
            InitCompletionHandler initCompletionHandler
    ) {
        progressText.textProperty().bind(task.messageProperty());
        loadProgress.progressProperty().bind(task.progressProperty());
        task.stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                loadProgress.progressProperty().unbind();
                loadProgress.setProgress(1);
                initStage.toFront();
                FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), splashLayout);
                fadeSplash.setFromValue(1.0);
                fadeSplash.setToValue(0.0);
                fadeSplash.setOnFinished(actionEvent -> initStage.hide());
                fadeSplash.play();
                initCompletionHandler.complete();
            }
        });

        Scene splashScene = new Scene(splashLayout, Color.TRANSPARENT);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        initStage.setScene(splashScene);
        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
        initStage.initStyle(StageStyle.TRANSPARENT);
        initStage.setAlwaysOnTop(false);
        initStage.getIcons().add(APPLICATION_ICON);
        initStage.show();
    }

    /**
     * Make sure the application finish on exiting the GUI
     */
    @Override
    public void stop() {
        System.exit(0);
    }

    public interface InitCompletionHandler {
        void complete();
    }
}
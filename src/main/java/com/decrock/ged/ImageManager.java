package com.decrock.ged;

import com.decrock.app.Config;
import com.decrock.database.Database;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class ImageManager {

    Database db;
    SimpleDateFormat sdf;
    String inseeCode = "69266";
    Pattern pattern;

    public ImageManager(Database database) {
        this.db = database;
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String regex = "\\.[A-Za-z]{3,4}$";
        pattern = Pattern.compile(regex);
    }

    public void classFile(File f, String user, String newDir) throws Exception {

        String imgInfos = db.getPhotoInfos(getCreationDate(f), user);

        File dir = new File(newDir + "\\" + imgInfos);
        if (!dir.exists())
            dir.mkdir();
        Matcher m = pattern.matcher(f.getName());
        if (!m.find()) throw new Exception("Extention de fichier " + f.getName() + "introuvable");
        String ext = m.group();
        String baseName = "P" + Config.getInstance().getCode_insee() + imgInfos;
        int count = 0;
        for (File file : dir.listFiles()) {
            if (file.getName().substring(0, baseName.length()).equals(baseName)) count++;
        }
        String newName = baseName + "_" + count + ext;
        Files.move(Paths.get(f.getAbsolutePath()), Paths.get(dir.getAbsolutePath() + "\\" + newName));
    }

    public Date getCreationDate(File f) throws IOException, ImageProcessingException {
        Metadata metadata = ImageMetadataReader.readMetadata(f);
        System.out.println(metadata);
        ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
        Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_DIGITIZED, TimeZone.getDefault());
//        System.out.println(date);
        return date;
    }

    public int getFileNumber(File dir) {
        if (!dir.exists()) return 0;
        return dir.listFiles().length;
    }

    public void classFiles(List<File> filesList, String user, String newDir) throws Exception {
        for (File file : filesList) classFile(file, user, newDir);
    }

}

package com.decrock.ged;

import com.decrock.app.Config;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Gestion Electronique de Document
 * Handles the methods relative to documents searches
 */
public class GED {
    // s
    private final Logger LOGGER = Logger.getLogger("GED");
    private Desktop desktop;
    private Pattern filePattern;
    private List<String> excludedTerms;
    private ResultSet resultSet;
    private Config config = Config.getInstance();

    public GED() {
        this.desktop = Desktop.getDesktop();
        filePattern = Pattern.compile(".*\\.[A-Za-z]{3,4}$");
        excludedTerms = new ArrayList<>();
        resultSet = new ResultSet();
        for (String word : config.getMots_ignores())
            excludedTerms.add(word.toLowerCase());
    }

    public ResultSet searchDoc(String[] keywordsList) throws IOException {
        return searchDoc(keywordsList, false);
    }

    public ResultSet searchDoc(String[] keywordsList, boolean exactNumbers) throws IOException {
        ResultSet emptyResultSet = new ResultSet();
        File dir = new File(config.getDocumentation_path());
        if (!dir.exists()) return emptyResultSet;

        emptyResultSet.setSearchPattern(keywordsList, excludedTerms, exactNumbers);
        if (resultSet.hasSameKeywords(emptyResultSet)) {
            return resultSet;
        }

        resultSet = emptyResultSet;
        for (File file : dir.listFiles()) {
            if (file.isFile() && (file.getName().substring(file.getName().length() - 4)).equals(".txt"))
                resultSet.searchFile(file);
        }
        return resultSet;
    }

    public ResultSet getPlotDoc(String address) throws IOException {
        if (address != null) {
            String[] keywordsList = address.split(" ");
            return searchDoc(keywordsList, true);
        }
        return new ResultSet();
    }

    public ResultSet getPlotDocLarge(String address) throws IOException {
        if (address != null) {
            String[] keywords = address.split(" ");
            List<String> keywords2 = new ArrayList<String>();
            for (int i = 0; i < keywords.length; i++)
                try {
                    Integer.valueOf(keywords[i]);
                } catch (NumberFormatException e) {
                    if (!keywords[i].toLowerCase().endsWith("bis") && !keywords[i].toLowerCase().endsWith("ter")) {
                        keywords2.add(keywords[i]);
                    }
                }
            return this.searchDoc(keywords2);
        }
        return null;
    }

    public ResultSet searchDoc(List<String> mcl) throws IOException {
        String[] keywords = mcl.toArray(new String[mcl.size()]);
        return searchDoc(keywords);
    }

    public void openFile(File file) throws IOException {
        {
            desktop.open(file);
        }
    }
}

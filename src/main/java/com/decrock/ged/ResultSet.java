package com.decrock.ged;

import com.decrock.app.Config;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handles the search for documents
 */
public class ResultSet {
    final private Pattern filePattern = Pattern.compile(".*\\.[A-Za-z]{3,4}$");
    private List<String> keywords;
    private List<FileDoc> results;
    private Pattern searchPattern;

    public ResultSet(List<String> keywords) {
        this.keywords = keywords;
        this.results = new LinkedList<>();
    }

    public ResultSet() {
        this.keywords = new ArrayList<>();
        this.results = new LinkedList<>();
    }

    public void addResult(FileDoc doc) {
        this.results.add(doc);
    }

    public void addKeyword(String keyword) {
        this.keywords.add(keyword);
    }

    public List<FileDoc> getResults() {
        return results;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public boolean hasSameKeywords(ResultSet rs) {
        if (keywords.isEmpty() || rs.getKeywords().isEmpty()) return false;
        if (rs.getKeywords().size() != keywords.size()) return false;
        for (String s : rs.getKeywords()) {
            if (!keywords.contains(s)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String keywords = "";
        for (String keyword : this.keywords) {
            keywords += " ; " + keyword;
        }
        if (keywords.length() > 3) keywords = keywords.substring(3);
        return "Mots recherchés : " + keywords + " - " + results.size() + " résultat(s)";
    }

    public boolean isEmpty() {
        return results.isEmpty();
    }

    public void setSearchPattern(String[] keywordsList, List<String> excludedTerms) {
        setSearchPattern(keywordsList, excludedTerms, false);
    }

    public void setSearchPattern(String[] keywordsList, List<String> excludedTerms, boolean exactNumbers) {
        List<String> keywordsPaternsList = new ArrayList<>(keywordsList.length);
        for (String keyword : keywordsList) {
            if (!excludedTerms.contains(keyword.toLowerCase())) {
                keywords.add(keyword.trim());
                String regexKeyword;
                try {
                    Integer.valueOf(keyword);
                    if (exactNumbers) regexKeyword = String.format("\\d*%s\\d*", keyword);
                    else regexKeyword = keyword;
                } catch (NumberFormatException e) {
                    regexKeyword = keyword.trim().replace("(", "\\(")
                            .replace(")", "\\)")
                            .replace("[", "\\[")
                            .replace("]", "\\]");

                }
                keywordsPaternsList.add(String.format("(?=.*%s)", regexKeyword));
            }
        }
        searchPattern = Pattern.compile("(REF|PAGE)\\R(.*)\\R(MCL)\\R"
                + String.join("", keywordsPaternsList)
                + "(.*)(\\R(TXT)\\R(([\\s\\S]*?)))?(?=(\\s*\\R){2,})", Pattern.CASE_INSENSITIVE);
    }


    public void searchFile(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, "Cp1252");
        BufferedReader br = new BufferedReader(isr);
        StringBuilder fileText = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            fileText.append(line).append("\n");
        }
        br.close();

        Matcher matcher = searchPattern.matcher(fileText);

        // Find all matches
        int i = 0;
        while (matcher.find()) {
            i++;
//            for (int i = 0; i < matcher.groupCount(); i++) {
//                System.out.println("indx : " + i);
//                System.out.println(matcher.group(i));
//            }

            String type = matcher.group(1);
            String ref = matcher.group(2);
            String mcl = matcher.group(4);
            String txt = "";
            if (matcher.groupCount() > 6) txt = matcher.group(7);
            if (txt == null) txt = "";

            if (type.equals("REF") && filePattern.matcher(ref).find()) {
                File refFile = new File(Config.getInstance().getDocumentation_path() + "/doc/" + ref);
                if (refFile.exists()) addResult(new FileDoc(refFile, ref, mcl, txt));
                else addResult(new FileDoc(ref, "", mcl, txt));
            } else if (type.equals("PAGE")) addResult(new FileDoc(file.getName(), ref, mcl, txt));
            else addResult(new FileDoc(ref, "", mcl, txt));

        }
    }

    public void findDiff(ResultSet rs) {
        A:
        for (FileDoc f : rs.getResults()) {
            for (FileDoc f2 : results) {
                if (f.equals(f2))
                    continue A;
            }
            System.out.println(f);
        }
    }

}

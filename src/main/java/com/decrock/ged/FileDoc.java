package com.decrock.ged;

import java.io.File;

/**
 * Contains the information for the result of a document search
 */
public class FileDoc {

    private File file;
    private String ref;
    private String mcl;
    private String txt;
    private String page;

    public FileDoc(File file, String ref, String mcl, String txt) {
        this.file = file;
        this.ref = ref;
        this.page = "";
        this.mcl = mcl;
        this.txt = txt;
    }

    public FileDoc(String ref, String page, String mcl, String txt) {
        this.file = null;
        this.ref = ref;
        this.page = page;
        this.mcl = mcl;
        this.txt = txt;
    }

    public File getFile() {
        return file;
    }

    public String getMcl() {
        return mcl;
    }

    public String getTxt() {
        return txt;
    }

    public String getRef() {
        return ref;
    }

    public String getPage() {
        return page;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        return String.valueOf(str.append("REF ").append(ref)
                .append("\nPAGE ").append(page)
                .append("\nMCL ").append(mcl)
                .append("\nTXT ").append(txt));
    }


    public boolean equals(FileDoc f) {
        return ref.equals(f.getRef())
                && page.equals(f.getPage());
//                && mcl.equals(f.getMcl())
//                && txt.equals(f.getTxt());
    }
}
